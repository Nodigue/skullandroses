#include "hdr/communication.h"

//Fonctions générales
int send_message(SOCKET client, char *message) {
	//On envoie une chaine de caractère à la socket client
	if (send(client, message, MAX_SIZE, 0) < 0) {
		perror("send");
		return -1;
	}
	return 0;
}

void send_message_to_all(Game game, char *message) {
	int nb_players = game.nb_players;
	Player player;

	int i;
	
	//On envoie le message à tous les joueurs qui ne sont pas des bots
	for (i = 0; i < nb_players; i++) {
		player = game.player_list[i];
		if (!player.is_bot) send_message(player.socket, message);
	}
}

int wait_message(SOCKET client, char *message) {
	int recv_err = recv(client, message, MAX_SIZE, 0);

	//On réceptionne une chaine de caractère de la socket client
	if (recv_err == 0) {
		return 1;
	
	}else if (recv_err < 0) {
		perror("recv");		
		return -1;
	}
	return 0;
}

int send_int(SOCKET client, int int_send) {
	char buf[BUFFER_SIZE] = "";
 
	//On envoie un int converti en chaine de caractère à la socket client
	sprintf(buf, "%d", int_send);
	if (send(client, buf, BUFFER_SIZE, 0) < 0) {
		perror("send");
		return -1;
	}

	return 0;
}

void send_int_to_all(Game game, int int_send) {
	int nb_players = game.nb_players;
	Player player;

	int i;

	//On envoie un int à tous les joueurs qui ne sont pas des bots
	for (i = 0; i < nb_players; i++) {
		player = game.player_list[i];
		if (!player.is_bot) send_int(player.socket, int_send);
	}
}

int wait_int(SOCKET client, int *int_recv) {
	char buf[BUFFER_SIZE] = "";
	int recv_err = recv(client, buf, BUFFER_SIZE, 0);
	
	//On réceptionne une chaine de caractère de la socket client qu'on convertie en int

	if (recv_err == 0) {
		return 1;
	
	}else if (recv_err < 0) {
		perror("recv");		
		return -1;
	}
	
	*int_recv = atoi(buf);

	return 0;
}

//Fonctions pour envoyer les informations sur les joueurs et la partie
void send_player_connected(Game game) {
	int nb_players = game.nb_players;
	int i;

	//On envoie le nombre de joueurs connectés
	send_int_to_all(game, nb_players);

	//On envoie les informations sur les joueurs connectés
	for (i = 0; i < nb_players; i++) {
		send_message_to_all(game, game.player_list[i].pseudo);
		send_message_to_all(game, game.player_list[i].avatar);
			
		send_int_to_all(game, game.player_list[i].cn);
	}
}

void send_infos_player(SOCKET player_socket, Player player) {
	//On envoie toutes les informations sur le joueur (pseudo, numéro de carte...)
	send_message(player_socket, player.pseudo);
	send_int(player_socket, player.cn);
	send_int(player_socket, player.is_bot);
	send_int(player_socket, player.nb_hand);
	send_int(player_socket, player.nb_board);
	send_int(player_socket, player.round_w);
	send_int(player_socket, player.round_l);
}

void send_infos_players(Game game) {
	int nb_players = game.nb_players;
	Player player;

	int player_nb;
	int i, j;
	
	for (i = 0; i < nb_players; i++) {
		player = game.player_list[i];
		player_nb = identify_player_pseudo(game, player.pseudo);

		if (!player.is_bot) {
			//On envoie d'abord les informations sur le joueur (il sera à l'index 0 dans la liste des joueurs)
			send_int(player.socket, nb_players);
			send_infos_player(player.socket, player);

			//On envoie les informations sur les autres joueurs
			for (j = 0; j < nb_players; j++) {
				if (j != player_nb) {
					send_infos_player(player.socket, game.player_list[j]);
				}
			}
		}
	}
}

void send_phase(Game game) {
	//On envoie la phase en cours
	send_int_to_all(game, game.phase);
}

void send_turn(Game game) {
	//On envoie le numéro de carte du joueur qui doit jouer
	send_int_to_all(game, game.player_list[game.turn].cn);
}

void send_infos_game(Game game) {
	//On envoie les informations sur la phase et le tour
	send_phase(game);
	send_turn(game);
}

void signal_player(Game game) {
	int nb_players = game.nb_players;	
	Player player;
	
	int turn = game.turn; 
	int is_turn;	
	int i;

	//On signal le joueur qui doit jouer avec un 1 les autres recevront un 0
	for (i = 0; i < nb_players; i++) {
		player = game.player_list[i];

		if (i == turn) is_turn = 1;
		else is_turn = 0;
		
		if (!player.is_bot) send_int(player.socket, is_turn);
	}
}

void send_confirmation(Game game, int confirmation) {
	//On envoie la confirmation de phase
	send_int_to_all(game, confirmation);
}

//Fonctions pour les actions et les informations
void send_player_action(Game game, char *pseudo) {
	//On envoie le pseudo du joueur qui devait jouer
	send_message_to_all(game, pseudo);
}

void send_action(Game game, int action) {
	//On envoie une action
	send_int_to_all(game, action);
}

int get_action(Player player) {
	int action;
	//On récupère l'action (ce que le joueur a joué)
	wait_int(player.socket, &action);
	
	return action;
}

void send_info(Game game, int info) {
	//On envoie une information
	send_int_to_all(game, info);
}

//Fonctions pour gérer les erreurs
int handle_disconnection(Game *game, Player *player) {
	char pseudo[MAX_SIZE];
	int disconnection_player = 0;

	strcpy(pseudo, player->pseudo);
	
	/*N'ayant pas réussi à gèrer la déconnection (fermeture de la fenêtre par la croix) de tout les joueurs chaque tour
	Seul le joueur devant jouer peut quitter proprement*/

	//On récupère l'information sur une possible déconnection du joueur
	if (!player->is_bot) wait_int(player->socket, &disconnection_player);

	//S'il s'est déconnecté, on le change en bot et on le signale au autre
	if (disconnection_player) {
		change_to_bot(game, player);

		send_info(*game, 1);
		send_player_action(*game, pseudo);

	}else {
		send_info(*game, 0);
	}

	return disconnection_player;
}

//Fonctions spécifiques aux phases
void send_cards(Player player) {
	int nb_cards = player.nb_hand;	
	int type;

	int i;
	
	//Pour chaque cartes, on envoie son type
	for (i = 0; i < nb_cards; i++) {
		type = player.in_hand[i].type;
		send_int(player.socket, type);
	}
}

void send_bet_infos(Game game, Player *player) {
	/*On envoie les informations sur les paris. S'il est déjà couché on lui signale, si il peut parier on lui envoie
	le paris max actuelle sinon on lui dit qu'il se couche (de force)*/

	if (player->bet >= 0) {
		if (game.max_bet == get_max_bet_possible(game)) player->bet = -1;
		else player->bet = game.max_bet;

	}else player->bet = -2;

	send_int(player->socket, player->bet);
}
