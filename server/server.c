#include "hdr/server.h"

int main(int argc, char *argv[]) {
	//Server
	SOCKET server;
	SOCKADDR_IN sin;

	SOCKET clients[10];
	SOCKADDR_IN csin;
	int s_csin;
	
	int nb_clients = 0;

	int max_fd;
	fd_set readfs;

	int i;	
	char choice[MAX_SIZE];
	
	//User
	USER user_list[50];
	int nb_users;

	//Game
	Game game;

	//On initialise la socket serveur
	if (init_server(&server, &sin) < 0) return 1;

	while (1) {
		max_fd = update_FD(&readfs, server, clients, nb_clients);
		
		//On utilise la fonction select qui va attendre un message venant des sockets de la liste (readfs)
		select(max_fd + 1, &readfs, NULL, NULL, NULL);
	
		nb_users = read_users(user_list);

		//Si ca vient de la socket serveur c'est qu'un client veut se connecter, on l'accepte
		if (FD_ISSET(server, &readfs)) {

			s_csin = sizeof(csin);
			clients[nb_clients] = accept(server, (SOCKADDR *) &csin, &s_csin);
		
			if (clients[nb_clients] < 0) {
				perror("accept");
				return 1;
			}

			nb_clients++;

		}else {
			for (i = 0; i < nb_clients; i++) {

				//Si ca vient d'un client, c'est qu'on a reçu un message, on le gère
				if (FD_ISSET(clients[i], &readfs)) {

					//Si ce message vaut "", il y a eu une déconnection forcée (directement depuis la console par exemple), il faut fermer la socket
					if (wait_message(clients[i], choice) == 1) {
						close_client(clients, &nb_clients, i);
			
					//Sinon on gère le message
					}else {
						handle_client_choice(clients[i], user_list, nb_users, choice, &game);
					}
				}
			}
		}
	}

	close(server);

	return 0;
}

//Fonctions pour gèrer les sockets
int init_server(SOCKET *server, SOCKADDR_IN *sin) {
	int opt = 1;

	//Fonctions nécessaires à l'initialisation de la socket server
	*server = socket(AF_INET, SOCK_STREAM, 0);

	if (*server < 0) {
		printf("Erreur création socket\n");
		return -1;
	}

	//On règle les options
	if (setsockopt(*server, SOL_SOCKET, SO_REUSEADDR, (char *) &opt, sizeof(opt)) < 0) {
		printf("Impossible de modifier les options de la socket\n");		
		return -1;
	}

	//L'addresse, le port
	sin->sin_addr.s_addr = htonl(INADDR_ANY);
	sin->sin_family = AF_INET;
	sin->sin_port = htons(PORT);

	//On la "bind"
	if (bind(*server, (SOCKADDR *) sin, sizeof(*sin)) < 0) {
		printf("Impossible de bind la socket\n");
		return -1;
	}

	//On écoute sur ce port
	if (listen(*server, 5) < 0) {
		printf("Impossible d'écouter sur le port\n");
		return -1;
	}

	system("clear");
	printf("Ouverture du serveur Skull & R0ses sur le port %d\n", PORT);
	printf("En attente de joueur...\n");	

	return 0;
} 

int update_FD(fd_set *readfs, SOCKET server, SOCKET clients[], int nb_clients) {
	//On nettoie la liste de lecture et on y ajoute toute les sockets connectées (server + clients) 
	FD_ZERO(readfs);
	FD_SET(server, readfs);
		
	int max_fd = server + 1;
	int i;

	for (i = 0; i < nb_clients; i++) {
		FD_SET(clients[i], readfs);
		if (clients[i] > max_fd) max_fd = clients[i];
	}

	return max_fd;
}


void close_client(SOCKET *clients, int *nb_clients, int client_nb) {
	int i;
	
	//On ferme la socket du client
	close(clients[client_nb]);

	//On le décale dans la liste des sockets
	for (i = client_nb; i < *nb_clients - 1; i++) {
		clients[i] = clients[i + i];
	}

	*nb_clients = *nb_clients - 1;
}

//Fonctions pour récupérer les utilisateurs
int read_users(USER *user_list) {
	FILE *user_list_file = NULL;
	int i = 0;

	user_list_file = fopen("dat/user.dat", "r");
	
	//On ajoute à la liste des utilisateurs chaques tous ceux enregistrer, en les comptent en même temps
	while (fread(&user_list[i], sizeof(USER), 1, user_list_file)) i++;
	fclose(user_list_file);
	
	return i;
}

USER get_User(USER user_list[], int nb_users, char *pseudo, char *pswd) {
	USER temp_user;
	int i;
	
	//On récupère dans la liste l'utilisateur correspondant au mot de passe et au pseudo
	for (i = 0; i < nb_users; i++) {
		if (strcmp(pseudo, user_list[i].pseudo) == 0 && strcmp(pswd, user_list[i].pswd) == 0) temp_user = user_list[i];
	}

	return temp_user;
}

//Fonctions pour la connections
int login_test(USER user_list[], int nb_users, char *pseudo, char *pswd) {
	int login = 0;
	int i;

	//On cherche dans la liste si il existe un utilisateur correspondant au mot de passe et au pseudo
	for (i = 0; i < nb_users; i++) {
		if (strcmp(pseudo, user_list[i].pseudo) == 0 && strcmp(pswd, user_list[i].pswd) == 0) {
			login = 1;
		}
	}
	
	return login;
}

int login_client(SOCKET client, USER user_list[], int nb_users) {
	USER user;

	char pseudo[MAX_SIZE];
	char pswd[MAX_SIZE];

	//On recoit le pseudo et le mot de passe
	wait_message(client, pseudo);	
	wait_message(client, pswd);

	//Si il existe un utilisateur on envoie ses caractèristiques
	if (login_test(user_list, nb_users, pseudo, pswd) == 1) {
		send_int(client, 1);

		user = get_User(user_list, nb_users, pseudo, pswd);

		send_message(client, user.pseudo);
		send_message(client, user.avatar);
		send_int(client, user.nb_victory);
		send_int(client, user.nb_played);

	}else {
		send_int(client, 0);
	}

	return 0;
}

void register_client(SOCKET client, USER user_list[]) {
	FILE *user_list_file = NULL;
	
	USER newUser;
	char pseudo[MAX_SIZE];
	char pswd[MAX_SIZE];
	char avatar[MAX_SIZE];

	//On récupère le pseudo, le mot de passe et l'avatar
	wait_message(client, pseudo);	
	wait_message(client, pswd);
	wait_message(client, avatar);

	strcpy(newUser.pseudo, pseudo);
	strcpy(newUser.pswd, pswd);
	strcpy(newUser.avatar, avatar);

	newUser.nb_victory = 0;
	newUser.nb_played = 0;

	//On l'ajoute au fichier des utilisateurs
	user_list_file = fopen("dat/user.dat", "a");
	fwrite(&newUser, sizeof(USER), 1, user_list_file);

	fclose(user_list_file);
}

//Fonction pour gérer le choix du client
void handle_client_choice(SOCKET client, USER *user_list, int nb_users, char *choice, Game *game) {
	if(strcmp(choice, "login") == 0) {	
		login_client(client, user_list, nb_users);
							
	}else if (strcmp(choice, "register") == 0) {
		register_client(client, user_list);

	}else if (strcmp(choice, "start_game_solo") == 0) {
		init_game_solo(client, user_list, nb_users, game);

	}else if (strcmp(choice, "join_game_multi") == 0) {
		init_game_multi(client, game);

	}else if (strcmp(choice, "quit_game_multi") == 0) {
		quit_game_multi(client, game);

	}else if (strcmp(choice, "start_game_multi") == 0) {
		start_game_multi(user_list, nb_users, game);
	}
}

//Fonctions pour modifier les caractèristiques des utilisateurs
void add_play_to_user(USER *user_list, int nb_users, char *pseudo) {
	FILE *user_list_file = NULL;
	int i;

	//On réecrit la liste des utilisateurs dans le fichier en rajoutant une partie jouée à celui avec le même pseudo
	user_list_file = fopen("dat/user.dat", "w");

	for (i = 0; i < nb_users; i++) {
		if (strcmp(user_list[i].pseudo, pseudo) == 0) user_list[i].nb_played++;

		fwrite(&user_list[i], sizeof(USER), 1, user_list_file);
	}

	fclose(user_list_file);
}

void add_victory_to_user(USER *user_list, int nb_users, char *pseudo) {
	FILE *user_list_file = NULL;
	int i;

	//On réecrit la liste des utilisateurs dans le fichier en rajoutant une victoire à celui avec le même pseudo
	user_list_file = fopen("dat/user.dat", "w");

	for (i = 0; i < nb_users; i++) {
		if (strcmp(user_list[i].pseudo, pseudo) == 0) user_list[i].nb_victory++;

		fwrite(&user_list[i], sizeof(USER), 1, user_list_file);
	}

	fclose(user_list_file);
}

