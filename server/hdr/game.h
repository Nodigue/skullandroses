#ifndef _GAME_H_
#define _GAME_H_

#include "ids.h"

#include "server.h"
#include "communication.h"
#include "bot.h"

//Fonctions pour lancer la partie
void init_game_solo(SOCKET client, USER *user_list, int nb_users, Game *game);
void init_game_multi(SOCKET client, Game *game);
void quit_game_multi(SOCKET client, Game *game);
void start_game_multi(USER *user_list, int nb_users, Game *game);

//Fonctions pour modifier les joueurs
void add_player(Game *game, SOCKET socket, char *pseudo, char *avatar, int cn);
void add_bot(Game *game);

void remove_player(Game *game, Player player);
void change_to_bot(Game *game, Player *player) ;

int rand_cn(Game game);

void set_nb_players(Game *game, int nb_players);
void set_nb_bots(Game *game, int nb_bots);

//Fonctions pour fixer la phase, le tour et le paris max
void set_phase(Game *game, int phase);
void set_turn(Game *game, int turn);
void set_max_bet(Game *game, int max_bet);

//Fonctions d'initialisation
void init_game(Game *game);

void init_players(Game *game);
void init_bots_names(Game *game);
void init_skull_position(Game *game);
void init_cards(Game *game);
void init_round(Game *game);

int get_first_turn(Game *game);
void next_turn(Game *game);

//Fonctions pour identifier le joueur
int identify_player_socket(Game game, SOCKET socket);
int identify_player_pseudo(Game game, char *pseudo);
int identify_player_cn(Game game, int cn);

//Fonctions de jeu
void game_main(USER *listUser, int nb_user, Game *game);
void start_game(Game *game);

void phase_1(Game *game);
void handle_action_phase_1(Game *game, Player *player, int card_played);
void play_card(Game *game, Player *player, int card_nb);

void phase_2(Game *game);
void handle_action_phase_2(Game *game, Player *player, int bet);
int check_bet(Game game);
int get_bet_winner(Game game);

void phase_3(Game *game);
void handle_action_phase_3(Game *game, Player *player, int player_target_cn);
int flip_card(Game *game, Player *player_target);

void phase_4(Game *game);
void phase_5(Game *game);

void win_round(Game *game, Player *player);
void loose_round(Game *game, Player *player);
void win_game(Game *game, Player *player);

Player get_winner(Game game);

//Fonction utilitaire
char *cat_char(char *text_1, int nb, char *text_2);
int get_tot_cards_board(Game game);

#endif
