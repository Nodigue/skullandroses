#ifndef _IDS_H_
#define _IDS_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#define PORT 8889

#define MAX_SIZE 100
#define BUFFER_SIZE 10

typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;

typedef struct User USER;
struct User {
	char pseudo[MAX_SIZE];
	char pswd[MAX_SIZE];

	char avatar[MAX_SIZE];

	int nb_victory;
	int nb_played;
};

typedef struct Error Error;
struct Error {
	char pseudo[MAX_SIZE];
	int code;
};

typedef struct Card Card;
struct Card {
	int type; //(0 - Rose / 1 - Skull)
};

typedef struct Player Player;
struct Player {
	int socket;

	char pseudo[MAX_SIZE];
	char avatar[MAX_SIZE];

	int cn;
	
	int is_bot;

	int nb_hand;
	int skull_pos;
	Card in_hand[4];

	int nb_board;
	Card on_board[4];

	int bet;

	int round_w;
	int round_l;
};

typedef struct Game Game;
struct Game {
	int init;

	int nb_players;
	int nb_bots;

	Player player_list[6];
	Player winner;	

	int turn;
	int phase;

	int finished;
	int round_finished;

	int max_bet;
};

#endif
