#ifndef _COMMUNICATION_H_
#define _COMMUNICATION_H_

#include "ids.h"

#include "server.h"
#include "game.h"

//Fonctions générales
int send_message(SOCKET client, char *message);
void send_message_to_all(Game game, char *message);
int wait_message(SOCKET client, char *message);

int send_int(SOCKET client, int int_send);
void send_int_to_all(Game game, int int_send);
int wait_int(SOCKET client, int *int_recv);

//Fonctions pour envoyer les informations sur les joueurs et la partie
void send_player_connected(Game game);

void send_infos_player(SOCKET player_socket, Player player);
void send_infos_players(Game game);
void send_phase(Game game);
void send_turn(Game game);
void send_infos_game(Game game);

void signal_player(Game game);
void send_confirmation(Game game, int confirmation);

//Fonctions pour les actions et les informations
void send_player_action(Game game, char *pseudo);
void send_action(Game game, int action);
int get_action(Player player);

void send_info(Game game, int info);

//Fonction pour gérer les déconnections
int handle_disconnection(Game *game, Player *player);

//Fonctions spécifiques aux phases
void send_cards(Player player);
void send_bet_infos(Game game, Player *player);

#endif
