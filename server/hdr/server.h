#ifndef _SERVER_H_
#define _SERVER_H_

#include "ids.h"

#include "communication.h"
#include "game.h"

//Fonctions pour gèrer les sockets
int init_server(SOCKET *server, SOCKADDR_IN *sin);
int update_FD(fd_set *readfs, SOCKET server, SOCKET clients[], int nb_clients);

void close_client(SOCKET *clients, int *nb_clients, int client_nb);

//Fonctions pour récupérer les utilisateurs
int read_users(USER *user_list);
USER get_User(USER user_list[], int nb_users, char *pseudo, char *pswd);

//Fonctions pour la connections
int login_test(USER user_list[], int nb_users, char *pseudo, char *pswd);
int login_client(SOCKET client, USER user_list[], int nb_users);
void register_client(SOCKET client, USER user_list[]);

//Fonction pour gérer le choix du client
void handle_client_choice(SOCKET client, USER *user_list, int nb_users, char *choice, Game *game);

//Fonctions pour modifier les caractèristiques des utilisateurs
void add_play_to_user(USER *user_list, int nb_users, char *pseudo);
void add_victory_to_user(USER *user_list, int nb_users, char *pseudo);

#endif
