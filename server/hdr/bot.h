#ifndef _BOT_H_
#define _BOT_H_

#include "ids.h"
#include "game.h"

int bot_card_selection(Game game, int bot_nb);
int have_skull(Game game, int bot_nb);

int bot_bet_selection(Game game, int bot_nb);
int get_max_bet_possible(Game game);

int bot_flip_selection(Game game, int bot_nb);

int rand_nb(int min, int max);

#endif
