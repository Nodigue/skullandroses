#include "hdr/game.h"

//Fonctions pour lancer la partie
void init_game_solo(SOCKET client, USER *user_list, int nb_users, Game *game) {
	char pseudo[MAX_SIZE];
	int cn = 0;
	int nb_bots = 0;

	int i;
	
	init_game(game);

	wait_message(client, pseudo);
	wait_int(client, &cn);								
	add_player(game, client, pseudo, "", cn);

	wait_int(client, &nb_bots);
	for (i = 0; i < nb_bots; i++) add_bot(game);

	game_main(user_list, nb_users, game);
}

void init_game_multi(SOCKET client, Game *game) {
	char pseudo[MAX_SIZE];
	char avatar[MAX_SIZE];

	if (!game->init) init_game(game);

	if (game->nb_players < 6) {
		send_int(client, 1);

		wait_message(client, pseudo);
		wait_message(client, avatar);

		add_player(game, client, pseudo, avatar, rand_cn(*game));
		
		send_player_connected(*game);

	}else {
		send_int(client, 0);
	}
}

void quit_game_multi(SOCKET client, Game *game) {
	Player player = game->player_list[identify_player_socket(*game, client)];
	
	remove_player(game, player);
	if (game->nb_players == 0) game->init = 0;

	send_player_connected(*game);	
}

void start_game_multi(USER *user_list, int nb_users, Game *game) {
	send_int_to_all(*game, 0);
	game_main(user_list, nb_users, game);
}

//Fonctions pour modifier les joueurs
void add_player(Game *game, SOCKET socket, char *pseudo, char *avatar, int cn) {
	int nb_players = game->nb_players;

	game->player_list[nb_players].socket = socket;
	strcpy(game->player_list[nb_players].pseudo, pseudo);
	strcpy(game->player_list[nb_players].avatar, avatar);
	game->player_list[nb_players].cn = cn;

	game->player_list[nb_players].is_bot = 0;

	set_nb_players(game, nb_players + 1);
}

void add_bot(Game *game) {
	int nb_players = game->nb_players;
	int nb_bots = game->nb_bots;

	game->player_list[nb_players].cn = rand_cn(*game);
	game->player_list[nb_players].is_bot = 1;

	set_nb_players(game, nb_players + 1);
	set_nb_bots(game, nb_bots + 1);
}

void remove_player(Game *game, Player player) {
	int nb_players = game->nb_players;
	int nb_bots = game->nb_bots;

	int player_nb = identify_player_pseudo(*game, player.pseudo);
	int i;

	for (i = player_nb; i < nb_players - 1; i++) {
		game->player_list[i] = game->player_list[i + 1];
	}

	set_nb_players(game, nb_players - 1);
	if (player.is_bot) set_nb_bots(game, nb_bots - 1);
}

void change_to_bot(Game *game, Player *player) {
	int nb_bots = game->nb_bots;	
	char pseudo[MAX_SIZE];

	strcpy(pseudo, player->pseudo);
	strcat(pseudo, " (bot)");
	strcpy(player->pseudo, pseudo);

	player->is_bot = 1;

	set_nb_bots(game, nb_bots + 1);
}

int rand_cn(Game game) {
	int nb_players = game.nb_players;

	int done;
	int nb_rand;

	int i;

	do {
		done = 1;
		nb_rand = rand() % 6 + 1;

		for(i = 0; i < nb_players; i++) {
			if (game.player_list[i].cn == nb_rand) done = 0;
		}

	}while(done == 0);

	return nb_rand;
}

void set_nb_players(Game *game, int nb_players) {
	game->nb_players = nb_players;
}

void set_nb_bots(Game *game, int nb_bots) {
	game->nb_bots = nb_bots;
}

//Fonctions pour fixer la phase, le tour et le paris max
void set_phase(Game *game, int phase) {
	game->phase = phase;
}

void set_turn(Game *game, int turn) {
	game->turn = turn;
}

void set_max_bet(Game *game, int max_bet) {
	game->max_bet = max_bet;
}

//Fonctions d'initialisation
void init_game(Game *game) {
	game->init = 1;

	set_nb_players(game, 0);
	set_nb_bots(game, 0);

	game->finished = 0;
}

void init_players(Game *game) {
	int nb_players = game->nb_players;
	int i;

	init_skull_position(game);
	init_bots_names(game);

	for (i = 0; i < nb_players; i++) {
		game->player_list[i].round_w = 0;
		game->player_list[i].round_l = 0;
	}
}

void init_bots_names(Game *game) {
	int nb_players = game->nb_players;	
	Player player;	
	
	int counter = 1;
	int i;

	for (i = 0; i < nb_players; i++) {
		player = game->player_list[i];

		if (player.is_bot) {
			strcpy(game->player_list[i].pseudo, cat_char("bot_", counter, ""));
			counter++;
		}
	}
}

void init_skull_position(Game *game) {
	int nb_players = game->nb_players;
	int i;

	for (i = 0; i < nb_players; i++) {
		game->player_list[i].skull_pos = rand() % 4;
	}
}

void init_cards(Game *game) {
	int nb_players = game->nb_players;
	int nb_cards;
	int i, j;

	for (i = 0; i < nb_players; i++) {
		nb_cards = game->player_list[i].nb_hand;

		for (j = 0; j < nb_cards; j++) {
			if (j == game->player_list[i].skull_pos) game->player_list[i].in_hand[j].type = 1;
			else game->player_list[i].in_hand[j].type = 0;
		}
	}
}

void init_round(Game *game) {
	int nb_players = game->nb_players;
	int turn = get_first_turn(game);
	int i;

	game->round_finished = 0;

	set_phase(game, 1);
	set_turn(game, turn);
	set_max_bet(game, 0);

	for (i = 0; i < nb_players; i++) {
		game->player_list[i].nb_hand = 4 - game->player_list[i].round_l;
		game->player_list[i].nb_board = 0;
		game->player_list[i].bet = 0;
	}
}

int get_first_turn(Game *game) {
	int nb_players = game->nb_players;
	return 0; //rand() % nb_players;
}

void next_turn(Game *game) {
	int turn = game->turn;
	int nb_players = game->nb_players;

	if (turn < nb_players - 1) turn++;
	else turn = 0;

	set_turn(game, turn);
}

//Fonctions pour identifier le joueur
int identify_player_socket(Game game, SOCKET socket) {
	int nb_players = game.nb_players;
	int player_nb = 0;
	int i;

	for (i = 0; i < nb_players; i++) {
		if (game.player_list[i].socket == socket) player_nb = i;
	}
	
	return player_nb;
}

int identify_player_pseudo(Game game, char *pseudo) {
	int nb_players = game.nb_players;
	int player_nb = 0;
	int i;

	for (i = 0; i < nb_players; i++) {
		if (strcmp(pseudo, game.player_list[i].pseudo) == 0) player_nb = i;
	}
	
	return player_nb;
}

int identify_player_cn(Game game, int cn) {
	int nb_players = game.nb_players;
	int player_nb = 0;
	int i;

	for (i = 0; i < nb_players; i++) {
		if (game.player_list[i].cn == cn) player_nb = i;
	}
	
	return player_nb;
}

//Fonctions de jeu
void game_main(USER *listUser, int nb_user, Game *game) {
	USER user;
	char pseudo[MAX_SIZE];
	int i;

	srand(time(NULL));

	//On ajoute une partie jouée à chaque joueur
	for (i = 0; i < game->nb_players; i++) {
		strcpy(pseudo, game->player_list[i].pseudo);

		if (!game->player_list[i].is_bot) {
			add_play_to_user(listUser, nb_user, pseudo);
		}
	}

	//On lance la partie
	start_game(game);

	//On ajoute une victoire au gagnant
	strcpy(pseudo, get_winner(*game).pseudo);

	if (!get_winner(*game).is_bot) {
		add_victory_to_user(listUser, nb_user, pseudo);
	}

	game->init = 0;
}

void start_game(Game *game) {
	init_players(game);
	
	do {
		init_round(game);
		init_cards(game);

		do {
			send_infos_players(*game);
			send_infos_game(*game);

			signal_player(*game);
			
			switch (game->phase) {
				case 1:
					phase_1(game);
					break;
				case 2:
					phase_2(game);
					break;
				case 3:
					phase_3(game);
					break;
				case 4:
					phase_4(game);
					break;
				case 5:
					phase_5(game);
					break;
			}

			//On envoie si la partie est finie ou non
			send_info(*game, game->finished);
		
		}while (!game->round_finished);
	}while (!game->finished);

	printf("%s gagne la partie !\n", game->winner.pseudo);
}

void phase_1(Game *game) {
	int turn = game->turn;

	Player player = game->player_list[turn];
	int card_played = -1;
	int disconnection_player = 0;

	if (player.nb_hand > 0) {
		//Le joueur peut jouer, on confirme la phase
		send_confirmation(*game, 1);

		if (player.is_bot) {
			card_played = bot_card_selection(*game, turn);

		}else {
			//On envoie au joueur les informations sur ses cartes et on récupère la carte qu'il a joué
			send_cards(player);
			card_played = get_action(player);
		}

	}else {
		//Le joueur ne peut pas jouer, on ne confirme pas la phase
		send_confirmation(*game, 0);
	}

	//On gère un possible déconnection du joueur avant de gérer les actions
	disconnection_player = handle_disconnection(game, &game->player_list[turn]);
	if (!disconnection_player) handle_action_phase_1(game, &game->player_list[turn], card_played);
}

void handle_action_phase_1(Game *game, Player *player, int card_played) {
	char pseudo[MAX_SIZE];	
	int message_code;

	strcpy(pseudo, player->pseudo);

	//Il a joué une carte
	if (card_played >= 0 && card_played < 4) {
		play_card(game, player, card_played);	
		next_turn(game);
		message_code = 1;

	//Il a lancé les paris
	}else if (card_played == 4) {		
		set_phase(game, 2);
		message_code = 2;
	
	//C'est qu'il n'y a pas eu de confirmation
	}else {
		set_phase(game, 2);
		message_code = 3;
	}

	//On envoie les informations de l'action
	send_player_action(*game, pseudo);	
	send_info(*game, message_code);
}

void play_card(Game *game, Player *player, int card_nb) {
	int nb_board = player->nb_board;
	int type, i;

	//On donne la valeur de la carte choisie à la dernière carte posée sur le plateau
	player->on_board[nb_board] = player->in_hand[card_nb];
	
	//On décale le reste des cartes en main
	for (i = card_nb; i < player->nb_hand - 1; i++) {
		player->in_hand[i] = player->in_hand[i + 1];
	}

	//On modifie le nombre de carte en main et sur le plateau
	player->nb_board++;
	player->nb_hand--;
}

void phase_2(Game *game) {
	int turn = game->turn;

	Player player = game->player_list[turn];
	int bet = 0;
	int disconnection_player;
	
	//Si des joueurs peuvent encore surencherir
	if (!check_bet(*game)) {
		send_confirmation(*game, 1);

		if (player.is_bot) {
			bet = bot_bet_selection(*game, turn);

		}else {
			//On envoie les conditions du paris et on récupère le paris du joueur
			send_bet_infos(*game, &game->player_list[turn]);
			bet = get_action(player);
		}

	//Un joueur a gagné les paris, il devient challengeur, on change de phase
	}else {
		send_confirmation(*game, 0);
	}

	//On gère les déconnections
	disconnection_player = handle_disconnection(game, &game->player_list[turn]);
	if (!disconnection_player) handle_action_phase_2(game, &game->player_list[turn], bet);
}

void handle_action_phase_2(Game *game, Player *player, int bet) {
	char pseudo[MAX_SIZE];	
	int message_code;	
	
	//Si le joueur a fait un paris, il y a eu une confirmation
	if (bet != 0) {
		if (bet > game->max_bet) game->max_bet = bet;
		player->bet = bet;

		next_turn(game);
	
		//Il faut envoyer le pseudo de celui qui a joué
		strcpy(pseudo, player->pseudo);

	//Il n'y a pas eu de confirmation
	}else {
		//On change la phase et le tour		
		set_phase(game, 3);
		set_turn(game, get_bet_winner(*game));

		//Il faut envoyer le pseudo du challengeur
		strcpy(pseudo, game->player_list[get_bet_winner(*game)].pseudo);
	}

	//On envoie le code du message associé	
	switch (bet) {
		case 0:
			message_code = 4;	//Remporte les paris
			break;
		case -1:
			message_code = 2;	//Se couche
			break;
		case -2:
			message_code = 3;	//Est déjà couché
			break;
		default:
			message_code = 1;	//Paris
			break;
	}

	//On envoie les informations de l'action aux clients
	send_player_action(*game, pseudo);
	send_action(*game, bet);	
	send_info(*game, message_code);
}

int check_bet(Game game) {
	int bet;	
	int pass = 0;
	int i;

	//On compte le nombre de joueur qui sont couchés
	for (i = 0; i < game.nb_players; i++) {
		bet = game.player_list[i].bet;
		if (bet < 0) pass += 1;
		else if (bet == 0) pass = -1;	//Un joueur n'a pas parié
	}
	
	//Si tout les joueurs sont couchés sauf un, il y a un challengeur 
	if (pass == game.nb_players - 1) return 1;
	else return 0;
}

int get_bet_winner(Game game) {
	int max_bet = game.max_bet;
	int winner;	
	int i;

	//On cherche le challengeur (celui qui a fait le plus grand paris) et on renvoie son numéro
	for (i = 0; i < game.nb_players; i++) {
		if (game.player_list[i].bet == max_bet) winner = i;
	}

	return winner;
}

void phase_3(Game *game) {
	int turn = game->turn;
	Player player = game->player_list[turn];

	int player_target_cn;
	int card_type;
	
	int disconnection_player;

	//On envoie le nombre de carte à retourner à tous le monde (pour l'affichage)
	send_action(*game, game->max_bet);

	if (player.is_bot) {
		player_target_cn = bot_flip_selection(*game, turn);
		
	}else {
		//Comme l'ordre des joueurs varie en fonction des joueurs, on passe par le numéro de carte (cn)
		player_target_cn = get_action(player);
	}

	//On gère les déconnections
	disconnection_player = handle_disconnection(game, &game->player_list[turn]);
	if (!disconnection_player) handle_action_phase_3(game, &game->player_list[turn], player_target_cn);
}

void handle_action_phase_3(Game *game, Player *player, int player_target_cn) {
	char pseudo[MAX_SIZE];
	int message_code;

	//On récupère le type de la carte retournée
	int player_target_nb = identify_player_cn(*game, player_target_cn);
	int card_type = flip_card(game, &game->player_list[player_target_nb]);

	//On envoi le pseudo du joueur, de la cible et le type de carte
	strcpy(pseudo, player->pseudo);
	send_player_action(*game, pseudo);
	
	send_action(*game, player_target_cn);
	send_action(*game, card_type);

	//Si la carte est une rose
	if (card_type == 0) {
		message_code = 1;

		//Si il n'y a plus de carte à retourner, on passe à la phase de victoire
		if (game->max_bet == 0) set_phase(game, 4);

	//Si c'est un skull
	}else {
		message_code = 2;

		//On passe à la phase de défaite
		set_phase(game, 5);
	}

	//On envoie le code du message
	send_info(*game, message_code);
}

int flip_card(Game *game, Player *player_target) {
	int nb_board_target = player_target->nb_board;
	int card_type = player_target->on_board[nb_board_target - 1].type;

	//On diminue le nombre de carte sur le plateau et le max_bet (correspondant au nombre de carte à retourner)
	player_target->nb_board--;
	game->max_bet--;
	
	//On renvoie le type de la carte retournée
	return card_type;
}

void phase_4(Game *game) {
	int message_code;	

	int turn = game->turn;
	Player player = game->player_list[turn];

	send_player_action(*game, player.pseudo);

	//Si le joueur n'a pas encore gagné de round
	if (player.round_w == 0) {
		win_round(game, &game->player_list[turn]);
		message_code = 1;

	//Si le joueur a déjà gagné un round
	}else if (player.round_w == 1) {
		win_game(game, &game->player_list[turn]);
		message_code = 2;
	}

	send_info(*game, message_code);		//Message code
}

void phase_5(Game *game) {
	int turn = game->turn;
	Player player = game->player_list[turn];

	send_player_action(*game, player.pseudo);

	loose_round(game, &game->player_list[turn]);
}

void win_round(Game *game, Player *player) {
	//On augmente son nombre de round gagné
	player->round_w++;

	//On finit le round
	game->round_finished = 1;
}

void loose_round(Game *game, Player *player) {
	//On augmente son nombre de round perdu
	player->round_l++;

	//Le joueur a encore des cartes
	if (player->round_l < 1) {
		send_info(*game, 1);

		//Le joueur n'est pas éliminé
		send_info(*game, 0);
		
		//On finit le round
		game->round_finished = 1;

	//Il ne lui restait qu'une carte, il est éliminé
	}else {
		send_info(*game, 2);
		//On previent le joueur qu'il est éliminé
		signal_player(*game);

		remove_player(game, *player);
		
		//Il ne reste plus qu'un joueur : il gagne par défaut
		if (game->nb_players == 1) {
			//On attribue au joueur restant une victoire et on passe directement en phase 4
			game->player_list[0].round_w = 1;

			set_phase(game, 4);
			set_turn(game, 0);
		
		}else {
			//On finit le round
			game->round_finished = 1;	
		}
	}
}

void win_game(Game *game, Player *player) {
	//On augmente son nombre de round gagné
	player->round_w++;

	//On finit la partie
	game->round_finished = 1;
	game->finished = 1;

	//On récupère le winner
	game->winner = get_winner(*game);
}

Player get_winner(Game game) {
	Player winner;
	int nb_players = game.nb_players;
	int i;

	//On cherche celui qui a 2 rounds de gagné
	for (i = 0; i < nb_players; i++) {
		if (game.player_list[i].round_w == 2) winner = game.player_list[i];
	}
	return winner;
}

//Fonction utilitaire
char *cat_char(char *text_1, int nb, char *text_2) {
	char final[MAX_SIZE] = "";
	char buf[BUFFER_SIZE] = "";
	
	if (strcmp(text_1, "") != 0) strcat(final, text_1);
	if (nb >= 0) {
		sprintf(buf, "%d", nb);
		strcat(final, buf);
	}
	if (strcmp(text_2, "") != 0) strcat(final, text_2);

	return strdup(final);
}

int get_tot_cards_board(Game game) {
	int nb_players = game.nb_players;	
	int total = 0;
	int i;

	for (i = 0; i < nb_players; i++) {
		total += game.player_list[i].nb_board;
	}
	
	return total;
}
