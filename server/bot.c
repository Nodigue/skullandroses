#include "hdr/bot.h"

int bot_card_selection(Game game, int bot_nb) {
	int nb_cards_board = get_tot_cards_board(game);
	int nb_cards = game.player_list[bot_nb].nb_hand;
	int card_played;

	int proba_1 = rand_nb(1, 100);	//Probabilité de jouer un skull
	int proba_2 = rand_nb(1, 100);	//Probabilité de lancer les paris

	//Le bot a environ 1 chance sur 3 de jouer un skull s'il en a un encore en mains le reste du temps, il joue un rose
	if (proba_1 <= 35 && have_skull(game, bot_nb)) card_played = game.player_list[bot_nb].skull_pos;
	else card_played = rand_nb(0, nb_cards - 1);
	
	if (nb_cards_board >= game.nb_players && proba_2 <= nb_cards_board * 10 - 30) card_played = 4;

	return card_played;
}

int have_skull(Game game, int bot_nb) {
	int nb_cards = game.player_list[bot_nb].nb_hand;
	int have_skull = 0;
	int i;
	
	//On regarde si le bot a un skull encore en main
	for (i = 0; i < nb_cards; i++) {
		if (game.player_list[bot_nb].in_hand[i].type == 1) have_skull = 1;
	}

	return have_skull;
}

int bot_bet_selection(Game game, int bot_nb) {
	int bet = game.player_list[bot_nb].bet;

	int min = game.max_bet;
	int max = get_max_bet_possible(game);
	
	int new_bet;

	int proba = rand_nb(1, 100);

	//Si le bot est déjà couché il ne parie pas
	if (bet < 0) new_bet = bet; 

	//Si il peut parier
	else if (min < max) {

		//Le bot a environ une chance sur 3 de se coucher mais si le paris est trop haut, il se couche directement
		if (proba <= 35 || min > (3 * game.nb_players)) new_bet = -1;
		//Sinon il surenchérie de 1
		else new_bet = min + 1;
	
	}else new_bet = -1;

	return new_bet;
}

int get_max_bet_possible(Game game) {
	int nb_players = game.nb_players;
	int max_bet = 0;
	int i;
	
	//On compte le nombre de carte posée sur le board pour avoir le paris maximal
	for (i = 0; i < nb_players; i++) {
		max_bet += game.player_list[i].nb_board;
	}
	
	return max_bet;
}

int bot_flip_selection(Game game, int bot_nb) {
	int nb_players = game.nb_players;
	int player_target;

	//Si le bot a encore des cartes à retourner de son coté il le fait, sinon il choisit au hasard
	if (game.player_list[bot_nb].nb_board > 0) player_target = bot_nb;
	else {
		do {
			player_target = rand_nb(0, nb_players - 1);
		}while (player_target == bot_nb || game.player_list[player_target].nb_board == 0);
	}

	return game.player_list[player_target].cn;
}

int rand_nb(int min, int max) {
	if (min == max) return max;
	else return rand() % ((max + 1) - min) + min;
}
