#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

#define MAX_SIZE 100

typedef struct User USER;
struct User {
	char pseudo[MAX_SIZE];
	char pswd[MAX_SIZE];

	char avatar[MAX_SIZE];

	int nb_victory;
	int nb_played;
};

void init();
int read_users(USER listUser[]);
void display_users(USER listUser[], int nb_user);

int main(int argc, char *argv[]) {
	system("clear");

	USER listUser[MAX_SIZE];	
	int choice;
	int nb_user;
	
	char pseudo[MAX_SIZE];
	char pswd[MAX_SIZE];
	
	printf("1-Init\n2-Liste User\n\n--> ");	
	scanf("%d", &choice);

	if (choice == 1) {
		init();

	}else if (choice == 2) {
		nb_user = read_users(listUser);
		display_users(listUser, nb_user);
	}

	return 0;
}

void init() {
	FILE *listUser_file = NULL;
	USER newUser;

	strcpy(newUser.pseudo, "admin");	
	strcpy(newUser.pswd, "admin");	
	strcpy(newUser.avatar, "res/img/avatars/avatar_0.bmp");
	
	newUser.nb_victory = 0;
	newUser.nb_played = 0;

	listUser_file = fopen("user.dat", "a");
	fwrite(&newUser, sizeof(USER), 1, listUser_file);

	fclose(listUser_file);
}

int read_users(USER listUser[]) {
	FILE *listUser_file = NULL;
	int i = 0;

	listUser_file = fopen("dat/user.dat", "r");
	while (fread(&listUser[i], sizeof(USER), 1, listUser_file) && !feof(listUser_file)) i++;
	fclose(listUser_file);
	
	return i;
}

void display_users(USER listUser[], int nb_user) {
	int i;

	for (i = 0; i < nb_user; i++) {
		printf("Pseudo : %s\nMot de passe : %s\n\n", listUser[i].pseudo, listUser[i].pswd);
	}
}
