#include "hdr/communication.h"

//Fonctions générales
int send_message(SOCKET server, char *message) {
	//On envoie une chaine de caractère à la socket server
	if (send(server, message, MAX_SIZE, 0) < 0) {
		perror("send");
		return -1;
	}
	return 0;
}

int wait_message(SOCKET server, char *message) {
	int recv_err = recv(server, message, MAX_SIZE, 0);

	//On réceptionne une chaine de caractère de la socket server
	if (recv_err == 0) {
		return 1;
	
	}else if (recv_err < 0) {
		perror("recv");		
		return -1;
	}
	
	return 0;
}

int send_int(SOCKET server, int int_send) {
	char buf[BUFFER_SIZE] = "";
 
	//On envoie un int converti en chaine de caractère à la socket server
	sprintf(buf, "%d", int_send);
	if (send(server, buf, BUFFER_SIZE, 0) < 0) {
		perror("send");
		return -1;
	}

	return 0;
}

int wait_int(SOCKET server, int *int_recv) {
	char buf[BUFFER_SIZE] = "";
	int recv_err = recv(server, buf, BUFFER_SIZE, 0);

	//On réceptionne une chaine de caractère de la socket server qu'on convertie en int
	if (recv_err == 0) {
		return 1;
	
	}else if (recv_err < 0) {
		perror("recv");		
		return -1;
	}
	
	*int_recv = atoi(buf);

	return 0;
}

//Fonctions de lancement de partis (envoi des différents messages nécessaires)
void start_game_solo(SOCKET server, char *pseudo, int cn, int nb_bots) {
	//On envoie un message comme quoi on veut lancer une partie solo
	send_message(server, "start_game_solo");

	//On envoie notre pseudo, le numéro de carte et le nombre de bot
	send_message(server, pseudo);
	send_int(server, cn);	
	send_int(server, nb_bots);
}

int join_game_multi(SOCKET server, char *pseudo, char *avatar) {
	int confirmation;
	
	//On envoie un message comme quoi on veut lancer une partie multi et on attend une confirmation
	send_message(server, "join_game_multi");
	wait_int(server, &confirmation);

	//Si on a eu la confirmation on envoie le pseudo et l'avatar
	if (confirmation) {
		send_message(server, pseudo);
		send_message(server, avatar);
	}

	return confirmation;		
}

void update_player_list(SOCKET server, User user, Player *player_list, int *nb_players, int *leader) {
	int max_fd = server + 1;
	fd_set readfs;
	struct timeval timeout = {.tv_sec = 2, .tv_usec = 0};

	int i;

	/*Afin de ne pas rester bloqué dans l'attente des informations sur les joueurs connectés
	et pouvoir gèrer les events des joueurs (lancement partie, retour, quitte..), on utilise la fonction select
	comme ca si au bout de 2s le client n'a rien reçu du serveur, il passe à la suite et gère les events avant
	de reattendre 2s et ainsi de suite*/

	FD_ZERO(&readfs);
	FD_SET(server, &readfs);

	select(max_fd + 1, &readfs, NULL, NULL, &timeout);
	
	//Si on recoit quelque chose du serveur, on récupère les informations sur les joueurs connectés
	if (FD_ISSET(server, &readfs)) {
		wait_int(server, nb_players);
	
		for (i = 0; i < *nb_players; i++) {
			wait_message(server, player_list[i].pseudo);
			wait_message(server, player_list[i].avatar);
	
			wait_int(server, &player_list[i].cn);
		}

		//Le leader est le premier joueur de la liste (donc si le joueur à le même pseudo que le premier joueur de la liste, il est le leader
		if (strcmp(user.pseudo, player_list[0].pseudo) == 0) *leader = 1;
		else *leader = 0;
	}
}

//Fonctions pour la récupération des informations générales de la partie
void get_infos_players(SOCKET server, Game *game) {
	int nb_players;
	
	char pseudo[MAX_SIZE];
	int cn, is_bot, nb_hand, nb_board, round_w, round_l;
	int i;

	//On récupère le nombre de joueur
	wait_int(server, &nb_players);
	game->nb_players = nb_players;

	//On récupère toutes les informations sur les joueurs (pseudo, numéro de carte, si c'est un bot...)
	for (i = 0; i < nb_players; i++) {
		wait_message(server, pseudo);
		wait_int(server, &cn);
		wait_int(server, &is_bot);
		wait_int(server, &nb_hand);
		wait_int(server, &nb_board);
		wait_int(server, &round_w);
		wait_int(server, &round_l);

		//On met toute les informations dans une structure que l'on ajoute à la liste des joueurs
		game->player_list[i] = get_player(pseudo, cn, is_bot, nb_hand, nb_board, round_w, round_l);
	}
}

int get_phase(SOCKET server) {
	int phase;	

	//On récupère la phase en cours
	wait_int(server, &phase);
	return phase;
}

int get_turn(SOCKET server, Game game) {
	int player_turn_cn;
	int turn;
	int i;	

	//On récupère le numéro de carte du joueur devant jouer (les joueurs n'étant pas de le même ordre pour chacun);
	wait_int(server, &player_turn_cn);
	
	//On en déduit le tour actuelle en comparant les numéros de carte
	for (i = 0; i < game.nb_players; i++) {
		if (game.player_list[i].cn == player_turn_cn) turn = i;
	}

	return turn;
}

void get_infos_game(SOCKET server, Game *game) {
	//On récupère la phase et le tour en cours
	game->phase = get_phase(server);
	game->turn = get_turn(server, *game);
}

int is_my_turn(SOCKET server) {
	int my_turn;

	//On récupère l'information si c'est à notre tour de jouer ou non
	wait_int(server, &my_turn);
	return my_turn;
}

int get_confirmation(SOCKET server) {
	int confirmation;
	
	//On récupère l'information sur la confirmation
	wait_int(server, &confirmation);
	
	return confirmation;
}

//Fonctions pour les informations et les actions
int get_info(SOCKET server) {
	int info;
	wait_int(server, &info);

	return info;
}

void get_player_action(SOCKET server, char *pseudo) {
	//On récupère le pseudo du joueur qui a joué
	wait_message(server, pseudo);
}

void send_player_action(SOCKET server, char *pseudo) {
	//On envoie le pseudo du joueur qui a joué	
	send_message(server, pseudo);
}

int get_action(SOCKET server) {
	int action;

	//On récupère l'information sur l'action
	wait_int(server, &action);
	return action;
}

void send_action(SOCKET server, int action) {
	//On envoie l'action faite
	send_int(server, action);
}

//Fonctions pour les erreurs (déconnections)
void send_error(SOCKET server, int error) {
	//On envoie le code d'erreur correspondant à une possible déconnection
	send_int(server, error);
}

//Fonction(s) spécifique(s)
void get_cards(SOCKET server, Player *player) {
	int nb_cards = player->nb_hand;
	int card_type;
	int i;

	//Pour chaque carte, on récupère son type (0 : Rose / 1 : Skull)
	for (i = 0; i < nb_cards; i++) {
		wait_int(server, &card_type);
		player->in_hand[i].type = card_type;
	}
}
