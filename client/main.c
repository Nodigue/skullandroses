#include "hdr/main.h"

int main(int argc, char *argv[]) {
	SOCKET server;
	SOCKADDR_IN sin;

	User user;

	Game game;
	int choice;
	
	//On initialise la SDL
	SDL_Surface *screen = get_SDL_Surface();

	//Si on peut se connecter au server
	if (connection_to_server(&server, &sin) == 0) {
	
		do {
			// On passe par le menu de connection puis on navigue entre les différents mode de jeu
			choice = login_screen(server, screen, &user);

			if (choice == 1) choice = menu_navigation(server, &user, screen, &game);
			else if (choice == 2) choice = register_screen(server, screen);

		}while (choice != -1);

	}else {
		printf("Le serveur de jeu est indisponible...\n");
	}

	//On ferme la SDL
	quit_SDL();

	return 0;
}

//Fonctions pour la SDL
SDL_Surface *get_SDL_Surface() {
	SDL_Surface *screen = NULL;	

	SDL_Init(SDL_INIT_VIDEO);
	TTF_Init();
	
	screen = SDL_SetVideoMode(1000, 600, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);	
	SDL_WM_SetCaption("Skull & R0ses", NULL);
	SDL_WM_SetIcon(SDL_LoadBMP("res/img/bg.bmp"), NULL);

	return screen;
}

void quit_SDL() {
	TTF_Quit();
	SDL_Quit();
}

//Fonction navigation menu
int menu_navigation(SOCKET server, User *user, SDL_Surface *screen, Game *game) {
	int choice, choice_s, choice_m;

	//Tant que l'utilisateur n'a pas quitté, on reste dans les menus ou les phases de jeu
	do {
		choice = main_menu(server, *user, screen);

		if (choice == 1) {
			choice_s = game_solo_menu(server, user, screen, game);

			if (choice_s == 1) {
				game_main(server, screen, game);
				if (game->quit) choice = -1;
		
			}else if (choice_s == -1) choice = -1;

		}else if (choice == 2) {
			choice_m = game_multi_menu(server, user, screen, game);

			if (choice_m == 1) {
				game_main(server, screen, game);
				if (game->quit) choice = -1;

			}else if (choice_m == -1) choice = -1;

		}else if (choice == 3) {
			choice = -1;
		}

	}while(choice != -1);

	return choice;
}

//Fonctions d'affichage
void display_login_screen(SDL_Surface *screen, Button *button_list, char *pseudo, char *pswd) {
	clear_background(screen, 18, 18, 18);

	button_list[0] = create_button(screen, 500, 50, 400, 100);
	write_text_on_button(screen, pseudo, button_list[0]);

	button_list[1] = create_button(screen, 500, 50, 400, 300);
	write_text_on_button(screen, hide_text(pswd), button_list[1]);
	
	button_list[2] = create_button(screen, 150, 50, 840, 540);
	write_text_on_button(screen, "Log-In", button_list[2]);
	
	button_list[3] = create_button(screen, 200, 50, 10, 540);
	write_text_on_button(screen, "Register", button_list[3]);

	write_text(screen, "Pseudo : ", 100, 100);
	write_text(screen, "Mot de passe : ", 100, 300);

	SDL_Flip(screen);
}

void display_register_screen(SDL_Surface *screen, Button *button_list, int avatar_selected, char *pseudo, char *pswd) {
	int i, j, k;

	clear_background(screen, 18, 18, 18);

	button_list[0] = create_button(screen, 500, 50, 400, 70);
	write_text_on_button(screen, pseudo, button_list[0]);
	
	button_list[1] = create_button(screen, 500, 50, 400, 180);
	write_text_on_button(screen, pswd, button_list[1]);
	
	button_list[2] = create_button(screen, 150, 50, 840, 540);
	write_text_on_button(screen, "Valider", button_list[2]);
	
	button_list[3] = create_button(screen, 200, 50, 10, 540);
	write_text_on_button(screen, "Retour", button_list[3]);

	for (i = 0; i < 3; i++) {
		for (j = 0; j < 2; j++) {
			k = 4 + ((2 * i) + j);

			if (k == avatar_selected) button_list[k] = create_button_color(screen, 100, 100, 395 + (i * 110), 285 + (j * 110), 186, 255, 134);
			else button_list[k] = create_button(screen, 100, 100, 395 + (i * 110), 285 + (j * 110));

			display_image(screen, cat_char("res/img/avatars/avatar_", 1 + ((2 * i) + j), ".bmp"), 400 + (i * 110), 290 + (j * 110), 90, 90);
		}
	}

	write_text(screen, "Pseudo : ", 100, 70);
	write_text(screen, "Mot de passe : ", 100, 180);
	write_text(screen, "Avatar : ", 100, 290);
	
	SDL_Flip(screen);
}

void display_main_menu(SDL_Surface *screen, Button *button_list) {
	clear_background(screen, 18, 18, 18),

	button_list[0] = create_button(screen, 200, 50, (1000 / 2) - (200 / 2), 400);
	write_text_on_button(screen, "Solo", button_list[0]);
		
	button_list[1] = create_button(screen, 200, 40, (1000 / 2) - (200 / 2), 460);
	write_text_on_button(screen, "Multi", button_list[1]);
		
	button_list[2] = create_button(screen, 200, 40, (1000 / 2) - (200 / 2), 510);
	write_text_on_button(screen, "Quitter", button_list[2]);

	display_image(screen, "res/img/bg.bmp", 325, 0, 350, 347);
	
	SDL_Flip(screen);
}

void display_game_solo_menu(SDL_Surface *screen, Button *button_list, User user, int nb_bots, int cn) {
	clear_background(screen, 18, 18, 18);
	
	button_list[0] = create_button_color(screen, 30, 30, 620, 165, 18, 18, 18);
	write_text_on_button(screen, "<", button_list[0]);

	button_list[1] = create_button_color(screen, 30, 30, 970, 165, 18, 18, 18);
	write_text_on_button(screen, ">", button_list[1]);

	button_list[2] = create_button_color(screen, 50, 50, 235, 430, 18, 18, 18);
	write_text_on_button(screen, "+", button_list[2]);

	button_list[3] = create_button_color(screen, 50, 50, 35, 430, 18, 18, 18);
	write_text_on_button(screen, "-", button_list[3]);

	button_list[4] = create_button(screen, 150, 50, 840, 540);
	write_text_on_button(screen, "Play !", button_list[4]);

	button_list[5] = create_button(screen, 150, 50, 10, 540);
	write_text_on_button(screen, "Retour", button_list[5]);

	button_list[6] = create_button_color(screen, 40, 40, 140, 435, 18, 18, 18);
	write_text_on_button(screen, cat_char("", nb_bots, ""), button_list[6]);

	button_list[7] = create_button(screen, 120, 120, 30, 30);

	button_list[8] = create_button_color(screen, 300, 30, 670, 375, 18, 18, 18);
	write_text_on_button(screen, get_mat_name(cn), button_list[8]);

	write_text(screen, cat_char("Pseudo  : ", -1, user.pseudo), 30, 180);
	write_text(screen, cat_char("Victoires  : ", user.nb_victory, ""), 30, 230);
	write_text(screen, cat_char("Parties jouees  : ", user.nb_played, ""), 30, 280);
	
	write_text(screen, "Nombre de bots :", 30, 380);

	display_image(screen, user.avatar, 35, 35, 110, 110);
	display_image(screen, get_mat_front_path(cn), 660, 30, 300, 300);
	
	SDL_Flip(screen);
}

void display_game_multi_menu(SDL_Surface *screen, Button *button_list, Player *player_list, int nb_players, int leader) {
	int i;
	int yb, y;

	clear_background(screen, 18, 18, 18);

	for (i = 0; i < nb_players; i++) {
		yb = 10 + ((20 + 60) * i);
		y = 15 + ((20 + 60) * i);
		
		if (i == 0) button_list[i] = create_button_color(screen, 900, 60, 50, yb, 247, 211, 88);
		else button_list[i] = button_list[i] = create_button(screen, 900, 60, 50, yb);	

		display_image(screen, player_list[i].avatar, 55, y, 50, 50);
		write_text(screen, player_list[i].pseudo, 130, y);

		display_image(screen, get_mat_front_path(player_list[i].cn), 885, y, 50, 50);
	}

	if (leader)	{
		button_list[nb_players + 1] = create_button(screen, 150, 50, 840, 540);
		write_text_on_button(screen, "Play !", button_list[nb_players + 1]);
	}

	button_list[nb_players + 2] = create_button(screen, 150, 50, 10, 540);
	write_text_on_button(screen, "Retour", button_list[nb_players + 2]);

	SDL_Flip(screen);
}

//Fonctions des menus
int login_screen(SOCKET server, SDL_Surface *screen, User *user) {	
	Button button_list[4];

	SDL_Event event;
	int mx, my;
	
	char pseudo[MAX_SIZE] = "";
	char pswd[MAX_SIZE] = "";
	
	int quit = 0;

	do {
		//Affichage des différents boutons, textes et images
		display_login_screen(screen, button_list, pseudo, pswd);
	
		SDL_WaitEvent(&event);
		mx = event.motion.x;		//On récupère les coordonnées du pointeur
		my = event.motion.y;

		//Gestion des events
		if (event.type == SDL_QUIT) quit = -1;
		else if (event.type == SDL_MOUSEBUTTONUP) {
		
			//L'utilisateur a cliqué sur la zone de texte pseudo			
			if (check_button(mx, my, button_list[0])) {
				if (text_zone(screen, button_list[0], pseudo, 500, 50, 400, 100, 0) == -1)  quit = -1;
					
			//L'utilisateur a cliqué sur la zone de texte mot de passe
			}else if (check_button(mx, my, button_list[1])) {
				if (text_zone(screen, button_list[1], pswd, 500, 50, 400, 300, 1) == -1)  quit = -1;

			//L'utilisateur a cliqué le bouton login : vérifie avec le serveur si ses identifiants sont corrects
			}else if (check_button(mx, my, button_list[2])) {

				if (login(server, pseudo, pswd) == 1) {
					quit = 1;
					*user = get_User(server);

				}else {
					warning_message(screen, "Pseudo et/ou mot de passe incorrect", 0);
					SDL_Delay(2000);
				}
			
			//L'utilisateur a cliqué sur le bouton pour se s'inscrire
			}else if (check_button(mx, my, button_list[3])) quit = 2;
		}

	}while (quit == 0);

	return quit;
}

int register_screen(SOCKET server, SDL_Surface *screen) {
	Button button_list[10];

	SDL_Event event;
	int mx, my;

	char pseudo[MAX_SIZE] = "";
	char pswd[MAX_SIZE] = "";
	char avatar[MAX_SIZE] = "";

	int avatar_selected = 0;	

	int quit = 0;
	int i, j;

	do {
		//Affichage des différents boutons, textes et images
		display_register_screen(screen, button_list, avatar_selected, pseudo, pswd);

		SDL_WaitEvent(&event);
		mx = event.motion.x;		//On récupère les coordonnées du pointeur
		my = event.motion.y;

		//Gestion des events
		if (event.type == SDL_QUIT) quit = -1;
		else if (event.type == SDL_MOUSEBUTTONUP) {

			//L'utilisateur a cliqué sur la zone de texte pseudo
			if (check_button(mx, my, button_list[0])) {
				if (text_zone(screen, button_list[0], pseudo, 500, 50, 400, 70, 0) == -1)  quit = -1;

			//L'utilisateur a cliqué sur la zone de texte mot de passe
			}else if (check_button(mx, my, button_list[1])) {
				if (text_zone(screen, button_list[1], pswd, 500, 50, 400, 180, 0) == -1) quit = -1;					

			//L'utilisateur a cliqué sur le bouton pour s'inscrire : on vérifie que les champs ne sont pas vide et on créer un nouveau utilisateur
			}else if (check_button(mx, my, button_list[2])) {

				if (strcmp(pseudo, "") != 0 && strcmp(pswd, "") != 0 && strcmp(avatar, "") != 0) {
					create_new_user(server, pseudo, pswd, avatar);
					quit = 1;

				}else {
					warning_message(screen, "Veuillez remplir les champs", 80);
					SDL_Delay(2000);
				}

			//L'utilisateur a cliqué sur le bouton retour
			}else if (check_button(mx, my, button_list[3])) quit = 2;

			else {
				for (i = 0; i < 3; i++) {
					for (j = 0; j < 2; j++) {

						//L'utilisateur a cliqué sur un avatar
						if (check_button(mx, my, button_list[4 + ((2 * i) + j)])) {
							avatar_selected = 4 + ((2 * i) + j);
							strcpy(avatar, cat_char("res/img/avatars/avatar_", 1 + ((2 * i) + j), ".bmp"));
						}
					}
				}
			}
		}

	}while (quit == 0);

	return quit;
}

int main_menu(SOCKET server, User user, SDL_Surface *screen) {
	Button button_list[3];

	SDL_Event event;
	int mx, my;

	int quit = 0;

	do {
		//Affichage des différents boutons, textes et images
		display_main_menu(screen, button_list);

		SDL_WaitEvent(&event);
		mx = event.motion.x;		//On récupère les coordonnées du pointeur
		my = event.motion.y;

		//Gestion des events
		if (event.type == SDL_QUIT) quit = -1;
		else if (event.type == SDL_MOUSEBUTTONDOWN) {
			
			//L'utilisateur a cliqué sur le bouton solo
			if (check_button(mx, my, button_list[0])) quit = 1;

			//L'utilisateur a cliqué sur le bouton multi : on vérifie qu'il peut se connecter avant de le faire
			else if (check_button(mx, my, button_list[1])) {

				if (join_game_multi(server, user.pseudo, user.avatar)) quit = 2;
			
			//L'utilisateur a cliqué sur le bouton quitter
			}else if (check_button(mx, my, button_list[2])) quit = -1;
		}

	}while (quit == 0);	

	return quit;
}

int game_solo_menu(SOCKET server, User *user, SDL_Surface *screen, Game *game) {
	Button button_list[9];
		
	SDL_Event event;
	int mx, my;
	
	int nb_bots = 2;
	int cn = 1;
	
	int quit = 0;
	int i;
	
	do {
		//Affichage des différents boutons, textes et images
		display_game_solo_menu(screen, button_list, *user, nb_bots, cn);
		
		SDL_WaitEvent(&event);
		mx = event.motion.x;	//On récupère les coordonnées du pointeur
		my = event.motion.y;

		//Gestion des events
		if (event.type == SDL_QUIT) quit = -1;
		else if (event.type == SDL_MOUSEBUTTONDOWN) {
			
			//L'utilisateur a cliqué sur le bouton "changement de couleur vers la gauche"	
			if (check_button(mx, my, button_list[0]) && cn < 6) cn++;

			//L'utilisateur a cliqué sur le bouton "changement de couleur vers la droite"
			else if (check_button(mx, my, button_list[1]) && cn > 1) cn--;

			//L'utilisateur a cliqué sur le bouton "plus de bot"	
			else if (check_button(mx, my, button_list[2]) && nb_bots < 5) nb_bots++;

			//L'utilisateur a cliqué sur le bouton "moins de bot"
			else if (check_button(mx, my, button_list[3]) && nb_bots > 2) nb_bots--;

			//L'utilisateur a cliqué sur le bouton play : on lance une partie solo								
			else if (check_button(mx, my, button_list[4])) {
				start_game_solo(server, user->pseudo, cn, nb_bots);
				quit = 1;

			//L'utilisateur a cliqué sur le bouton retour
			}else if (check_button(mx, my, button_list[5])) quit = 2;
		}

	}while (quit == 0);

	return quit;
}

int game_multi_menu(SOCKET server, User *user, SDL_Surface *screen, Game *game) {
	Button button_list[6 + 2];

	SDL_Event event;
	int mx, my;

	Player player_list[6];
	int nb_players;
	int leader;	

	int quit = 0;
	int i;

	do {
		//Affichage des différents boutons, textes et images après actualisation de la liste des joueurs
		update_player_list(server, *user, player_list, &nb_players, &leader);
		display_game_multi_menu(screen, button_list, player_list, nb_players, leader);

		//Si le nombre de joueurs vaut 0 c'est que la partie a été lancée
		if (nb_players != 0) {
	
			//On gère tout les events avant de faire une nouvelle actualisation
			do {
				mx = event.motion.x;		//Récupération des coordonnées du pointeur
				my = event.motion.y;

				//L'utilisateur a quitté, on envoie un message au serveur
				if (event.type == SDL_QUIT) {
					quit = -1;
					send_message(server, "quit_game_multi");
	
				}else if (event.type == SDL_MOUSEBUTTONDOWN) {

					//Le leader (le seul pouvant lancer la partie) a cliqué sur le bouton play : on lance la partie s'il y a au moins 6 joueurs
					if (leader && check_button(mx, my, button_list[nb_players + 1]))  {

						if (nb_players >= 3) {						
							send_message(server, "start_game_multi");
				
						}else warning_message(screen, "Il faut au moins 3 joueurs dans une partie\n", 0);

					//L'utilisateur a cliqué sur le bouton retour, pour le serveur, il a quitté la partie
					}else if (check_button(mx, my, button_list[nb_players + 2])) {
						quit = 2;
						send_message(server, "quit_game_multi");
					}
				}
			}while (SDL_PollEvent(&event));
		
		}else quit = 1;

	}while (quit == 0);

	return quit;
}
