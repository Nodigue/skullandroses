#ifndef _GRAPHISM_H_
#define _GRAPHISM_H_

#include "ids.h"
#include "game.h"

//Fonctions générales
void clear_background(SDL_Surface *screen, int r, int g, int b);
void darken_background(SDL_Surface *screen);
int hover(int mx, int my, int x, int y, int w, int h);

//Fonctions pour les boutons
int get_X(Button button);
int get_Y(Button button);
int get_Width(Button button);
int get_Height(Button button);

void set_Coords(Button *button, int x, int y);
void set_Dimension(Button *button, int width, int height);
void set_Color(Button *button, int r, int g, int b);

void place_button(SDL_Surface *screen, Button button);
Button create_button(SDL_Surface *screen, int width, int height, int x, int y);
Button create_button_color(SDL_Surface *screen, int width, int height, int x, int y, int r, int g, int b);

int check_button(int mx, int my, Button button);

//Fonctions pour les images
double get_zoom_X(int wi, int w);
double get_zoom_Y(int hi, int h);

void display_image(SDL_Surface *screen, char *path, int x, int y, int width, int height);

void semi_flip(SDL_Surface *screen, char *path, int x, int y, int width, int height, int part);
void flip_card(SDL_Surface *screen, char *path_1, char *path_2, int x, int y, int width, int height);

//Fonctions pour le texte
void write_text_color_size(SDL_Surface *screen, char *text, int x, int y, int r, int g, int b, int size);
void write_text(SDL_Surface *screen, char *text, int x, int y);
void write_text_color(SDL_Surface *screen, char *text, int x, int y, int r, int g, int b);
void write_text_size(SDL_Surface *screen, char *text, int x, int y, int size);
void write_text_on_button(SDL_Surface *screen, char *text, Button button);

char *cat_char(char *text_1, int nb, char *text_2);
char *hide_text(char *text);

int enter_text(SDL_Surface *screen, Button button, char* text, int hidden);
int text_zone(SDL_Surface *screen, Button button, char *text, int w, int h, int x, int y, int hidden);

void warning_message(SDL_Surface *screen, char *text, int y_offset);
void action_message(SDL_Surface *screen, Game *game, char *text);

#endif
