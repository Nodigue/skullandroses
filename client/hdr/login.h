#ifndef _LOGIN_H_
#define _LOGIN_H_

#include "ids.h"

#include "communication.h"

int connection_to_server(SOCKET *server, SOCKADDR_IN *sin);

User get_User(SOCKET server);

int login(SOCKET server, char *pseudo, char *pswd);
void create_new_user(SOCKET server, char *pseudo, char *pswd, char *avatar);

#endif
