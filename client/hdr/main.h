#ifndef _MAIN_H_
#define _MAIN_H_

#include "ids.h"

#include "login.h"
#include "communication.h"

#include "graphism.h"
#include "game.h"

//Fonctions pour la SDL
SDL_Surface *get_SDL_Surface();
void quit_SDL();

//Fonction navigation menu
int menu_navigation(SOCKET server, User *user, SDL_Surface *screen, Game *game);

//Fonctions d'affichage
void display_login_screen(SDL_Surface *screen, Button *button_list, char *pseudo, char *pswd);
void display_register_screen(SDL_Surface *screen, Button *button_list, int avatar_selected, char *pseudo, char *pswd);

void display_main_menu(SDL_Surface *screen, Button *button_list);
void display_game_solo_menu(SDL_Surface *screen, Button *button_list, User user, int nb_bots, int cn);
void display_game_multi_menu(); 

//Fonctions des menus
int login_screen(SOCKET server, SDL_Surface *screen, User *user);
int register_screen(SOCKET server, SDL_Surface *screen);

int main_menu(SOCKET server, User user, SDL_Surface *screen);
int game_solo_menu(SOCKET server, User *user, SDL_Surface *screen, Game *game);
int game_multi_menu(SOCKET server, User *user, SDL_Surface *screen, Game *game);

#endif
