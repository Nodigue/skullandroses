#ifndef _IDS_H_
#define _IDS_H_

//Includes
#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <time.h>
#include <math.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_rotozoom.h>

//Defines
#define PORT 8889

#define MAX_SIZE 100
#define BUFFER_SIZE 10
#define pi 3.14159265

#define MAX_SIZE 100

//Structures
typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;

typedef struct User User;
struct User {
	char pseudo[MAX_SIZE];
	char pswd[MAX_SIZE];

	char avatar[MAX_SIZE];

	int nb_victory;
	int nb_played;
};

typedef struct Button Button;
struct Button {
	SDL_Rect position;
	SDL_Color color;
};

typedef struct Card Card;
struct Card {
	int type; //(0 - Rose / 1 - Skull)
};

typedef struct Player Player;
struct Player {
	char pseudo[MAX_SIZE];
	char avatar[MAX_SIZE];
	int cn; //Card Number
	
	int is_bot;

	int nb_hand;
	Card in_hand[4];

	int nb_board;
	Card on_board[4];

	int bet;

	int round_w;
	int round_l;
};

typedef struct Game Game;
struct Game {
	int finished;
	int quit;

	int nb_players;
	int nb_bots;

	Player player_list[6];
	
	int turn;
	int phase;

	int max_bet;
};

#endif
