#ifndef _GAME_H_
#define _GAME_H_

#include "ids.h"
#include "login.h"
#include "graphism.h"

//Fonctions d'affichage
void reset_board(SDL_Surface *screen, Game game);
void display_turn(SDL_Surface *screen, Game game);
void display_players(SDL_Surface *screen, Game game);
void display_cards_played(SDL_Surface *screen, Game game);

void display_cards_panel(SDL_Surface *screen, Game *game, Button *button_bet, int mx, int my, int card_selected);
void display_bet_panel(SDL_Surface *screen, Game *game, Button *button_list, int bet);
void display_flip_panel(SDL_Surface *screen, Game *game, int nb_to_flip);

void flip_animation(SDL_Surface *screen, Game *game, char *pseudo, int player_target_id, int card_type);

//Fonctions de jeu
void game_main(SOCKET server, SDL_Surface *screen, Game *game);

void phase_1(SOCKET server, SDL_Surface *screen, Game *game, int my_turn);
int update_card_selected(Game game, int mx, int my);
int card_selection(SDL_Surface *screen, Game *game, int *error);
int launch_bet_possible(Game game);

void phase_2(SOCKET server, SDL_Surface *screen, Game *game, int my_turn);
int update_bet(Game game, int mx, int my, Button *button_list, int info, int bet, int *done);
int bet_selection(SDL_Surface *screen, Game *game, int bet, int *error);
int bet_possible(Game game, int bet);

void phase_3(SOCKET server, SDL_Surface *screen, Game *game, int my_turn);
int flip_selection(SDL_Surface *screen, Game *game, int *error);

void final_phase(SOCKET server, SDL_Surface *screen, Game *game);

//Fonctions pour les messages d'actions
void message(SDL_Surface *screen, Game *game, char *pseudo, int message_code, int bet);
void phase_1_message(SDL_Surface *screen, Game *game, char *pseudo, int message_code);
void phase_2_message(SDL_Surface *screen, Game *game, char *pseudo, int message_code, int bet);
void phase_3_message(SDL_Surface *screen, Game *game, char *pseudo, int message_code);
void phase_4_message(SDL_Surface *screen, Game *game, char *pseudo, int message_code);
void phase_5_message(SDL_Surface *screen, Game *game, char *pseudo, int message_code);

//Fonctions utilitaires
void clear_event(SDL_Event *event);

int get_X_coord_player(int nb, int nb_players);
int get_Y_coord_player(int nb, int nb_players);

int get_X_coord_card(int nb, int nb_cards);
int get_Y_coord_card(int nb, int nb_cards);

int get_tot_cards_board(Game game);

char *get_mat_name(int cn);
char *get_mat_front_path(int cn);
char *get_mat_back_path(int cn);

char *get_card_back_path(int cn);
char *get_card_rose_path(int cn);
char *get_card_skull_path(int cn);
char *get_card_path(int cn, int type);

Player get_player(char *pseudo, int cn, int is_bot, int nb_hand, int nb_board, int round_w, int round_l);

#endif
