#ifndef _COMMUNICATION_H_
#define _COMMUNICATION_H_

#include "ids.h"

#include "login.h"
#include "game.h"

//Fonctions générales
int send_message(SOCKET server, char *message);
int wait_message(SOCKET server, char *message);
int send_int(SOCKET server, int int_send);
int wait_int(SOCKET server, int *int_recv);

//Fonctions de lancement de partis (envoi des différents messages nécessaires)
void start_game_solo(SOCKET server, char *pseudo, int cn, int nb_bots);
int join_game_multi(SOCKET server, char *pseudo, char *avatar);
void update_player_list(SOCKET server, User user, Player *player_list, int *nb_players, int *leader);

//Fonctions pour la récupération des informations générales de la partie
void get_infos_players(SOCKET server, Game *game);
int get_phase(SOCKET server);
int get_turn(SOCKET server, Game game);
void get_infos_game(SOCKET server, Game *game);
int is_my_turn(SOCKET server);

int get_confirmation(SOCKET server);

//Fonctions pour les informations et les actions
int get_info(SOCKET server);

void get_player_action(SOCKET server, char *pseudo);
void send_player_action(SOCKET server, char *pseudo);

int get_action(SOCKET server);
void send_action(SOCKET server, int action);

//Fonction pour les erreurs (déconnections)
void send_error(SOCKET server, int error);

//Fonction(s) spécifique(s)
void get_cards(SOCKET server, Player *player);

#endif
