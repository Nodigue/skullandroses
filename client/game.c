#include "hdr/game.h"

//Fonctions d'affichage
void reset_board(SDL_Surface *screen, Game game) {
	//On nettoie la fenêtre
	clear_background(screen, 224, 205, 169);	
	
	//On affiche la marque du tour, les différents joueurs et le nombre de cartes jouées
	display_turn(screen, game);
	display_players(screen, game);
	display_cards_played(screen, game);
}

void display_turn(SDL_Surface *screen, Game game) {
	int turn = game.turn;
	int x = get_X_coord_player(turn, game.nb_players) - 5;
	int y = get_Y_coord_player(turn, game.nb_players) - 5;

	Button mark = create_button_color(screen, 120, 120, x, y, 255, 0, 0);
}

void display_players(SDL_Surface *screen, Game game) {
	int nb_players = game.nb_players;	
	Player player;	

	int x, y;
	int i;

	for (i = 0; i < nb_players; i++) {
		player = game.player_list[i];
	
		x = get_X_coord_player(i, nb_players);
		y = get_Y_coord_player(i, nb_players);
		
		//Si le joueur n'a pas encore gagné de round, on affiche le recto du tapis
		if (player.round_w == 0)
			display_image(screen, get_mat_front_path(player.cn), x, y, 110, 110);
		
		//Sinon on affiche le verso		
		else 
			display_image(screen, get_mat_back_path(player.cn), x, y, 110, 110);
		
		if (i == 0) write_text(screen, player.pseudo, x + 110 + 10, y);
		else write_text(screen, player.pseudo, x, y + 110 + 10);
	}
}

void display_cards_played(SDL_Surface *screen, Game game) {
	int nb_players = game.nb_players;
	Player player;	

	int nb_cards_board;	

	int x, y;	
	int i, j;

	for (i = 0; i < nb_players; i++) {
		player = game.player_list[i];
		nb_cards_board = player.nb_board;

		x = get_X_coord_player(i, nb_players);
		y = get_Y_coord_player(i, nb_players);

		if (nb_cards_board > 0) display_image(screen, get_card_back_path(player.cn), x + 5, y + 5, 110 - 10, 110 - 10);

		if (i == 0) write_text(screen, cat_char("x ", nb_cards_board, ""), x + 110 + 10, y + 40);
		else write_text(screen, cat_char("x ", nb_cards_board, ""), x, y + 110 + 50);
	}
}

void display_cards_panel(SDL_Surface *screen, Game *game, Button *button_bet, int mx, int my, int card_selected) {
	Player player = game->player_list[0];
	int nb_cards = player.nb_hand;

	int w = 200, h = 200;	
	int x, y;
	int i;
	
	reset_board(screen, *game);
	darken_background(screen);

	*button_bet = create_button(screen, 160, 60, 830, 530);
	write_text_on_button(screen, "Defi ! ", *button_bet);

	//On affiche chaque carte, si une est sélectionnée : on l'affiche en plus grand
	for (i = 0; i < nb_cards; i++) {
		x = get_X_coord_card(i, nb_cards);
		y = get_Y_coord_card(i, nb_cards);
	
		if (card_selected == i) display_image(screen, get_card_path(player.cn, player.in_hand[i].type), x - 30, y - 30, w + 60, h + 60);
		else display_image(screen, get_card_path(player.cn, player.in_hand[i].type), x, y, w, h);
	}

	SDL_Flip(screen);
}

void display_bet_panel(SDL_Surface *screen, Game *game, Button *button_list, int bet) {
	reset_board(screen, *game);
	darken_background(screen);	

	display_image(screen, "res/img/bet_panel.png", 350, 150, 300, 300);
	
	button_list[0] = create_button(screen, 50, 50, 345, 230);
	write_text_on_button(screen, "-", button_list[0]);

	button_list[1] = create_button(screen, 50, 50, 600, 300);
	write_text_on_button(screen, "+", button_list[1]);

	button_list[2] = create_button(screen, 160, 40, 505, 445);
	write_text_on_button(screen, "Valider", button_list[2]);
	
	button_list[3] = create_button(screen, 160, 40, 335, 445);
	write_text_on_button(screen, "Se coucher", button_list[3]);

	write_text(screen, cat_char("", bet, ""), -1, -1);

	SDL_Flip(screen);
}

void display_flip_panel(SDL_Surface *screen, Game *game, int nb_to_flip) {
	reset_board(screen, *game);
	write_text(screen, cat_char("Carte(s) a retourner : ", nb_to_flip, ""), 10, 560);

	SDL_Flip(screen);
}

void flip_animation(SDL_Surface *screen, Game *game, char *pseudo, int player_target_id, int card_type) {
	char path_1[MAX_SIZE];
	char path_2[MAX_SIZE];

	strcpy(path_1, get_card_back_path(player_target_id));
	if (card_type) strcpy(path_2, get_card_skull_path(player_target_id));
	else strcpy(path_2, get_card_rose_path(player_target_id));

	flip_card(screen, path_1, path_2, 500, 350, 110, 110);
	SDL_Delay(1000);
}

//Fonctions de jeu
void game_main(SOCKET server, SDL_Surface *screen, Game *game) {
	SDL_Event event;

	int phase;
	int my_turn;
	int nb_cards;
	int i;

	game->finished = 0;
	game->quit = 0;
	
	do {
		//On récupère les informations sur la partie et les joueurs
		get_infos_players(server, game);
		get_infos_game(server, game);

		my_turn = is_my_turn(server);
	
		reset_board(screen, *game);
		SDL_Flip(screen);

		//On switch entre les différentes phases
		switch(game->phase) {
			case 1:
				phase_1(server, screen, game, my_turn);		//Sélection d'une carte à jouer
				break;
			case 2:
				phase_2(server, screen, game, my_turn);		//Sélection des paris
				break;
			case 3:
				phase_3(server, screen, game, my_turn);		//Retournement des cartes
				break;
			case 4:
				final_phase(server, screen, game);			//Victoire du round et/ou de la partie
				break;
			case 5:
				final_phase(server, screen, game);			//Défaite du round et/ou élimination
				break;
		
		}
		
		//On vérifie que la partie n'est pas terminée
		if (!game->finished && !game->quit) game->finished = get_info(server);

	}while (!game->finished && !game->quit);
}

void phase_1(SOCKET server, SDL_Surface *screen, Game *game, int my_turn) {
	char pseudo[MAX_SIZE];
	int card_played;

	int confirmation = get_confirmation(server);
	int error = 0;
	int disconnection_player;
	int message_code;

	if (my_turn) {

		if (confirmation) {
			//On récupère les informations sur nos cartes
			get_cards(server, &game->player_list[0]);
	
			//On choisit la carte que l'on veut jouer, et on l'envoie
			card_played = card_selection(screen, game, &error);
			send_action(server, card_played);
		}

		//On envoie l'erreur
		send_error(server, error);
	}

	//Si l'on a pas quitter le jeu
	if (!game->quit) {

		//On regarde si le joueur devant jouer n'a pas quitté
		disconnection_player = get_info(server);
	
		if (!disconnection_player) {
			//On récupère le pseudo du joueur qui a joué et le code du message
			get_player_action(server, pseudo);	
			message_code = get_info(server);
			
			//On affiche le message correspondant
			message(screen, game, pseudo, message_code, 0);

		}else {
			get_player_action(server, pseudo);			
			action_message(screen, game, cat_char(pseudo, -1, " quitte le jeu..."));
		}
	}
}

int update_card_selected(Game game, int mx, int my) {
	int nb_cards = game.player_list[0].nb_hand;
	int i;
	
	//On détecte si le pointeur est au dessus d'une carte
	for (i = 0; i < nb_cards; i++) {
		if (hover(mx, my, get_X_coord_card(i, nb_cards), get_Y_coord_card(i, nb_cards), 200, 200)) return i;				
	}
	return -1;
}

int launch_bet_possible(Game game) {
	int nb_players = game.nb_players;
	int nb_cards = 0;
	int i;

	//On peut parier seulement si tous les joueurs ont au moins une carte de posée
	for (i = 0; i < nb_players; i++) {
		nb_cards += game.player_list[i].nb_board;
	}

	if (nb_cards >= nb_players) return 1;
	else return 0;
}

int card_selection(SDL_Surface *screen, Game *game, int *error) {
	SDL_Event event;	
	int mx = 0, my = 0;

	int card_selected = -1, old_card_selected, card_played = -1;
	int i;

	Button button_bet;

	display_cards_panel(screen, game, &button_bet, mx, my, card_selected);

	//On nettoie la liste des events
	clear_event(&event);	

	do {
		SDL_WaitEvent(&event);
		mx = event.motion.x;	//On récupère les coordonnées du pointeur
		my = event.motion.y;
	
		old_card_selected = card_selected;
		card_selected = update_card_selected(*game, mx, my);
		
		//On actualise l'affichage seulement s'il y a un changement
		if (card_selected != old_card_selected) display_cards_panel(screen, game, &button_bet, mx, my, card_selected);

		//Gestion des events
		if (event.type == SDL_MOUSEBUTTONDOWN) {
			if (check_button(mx, my, button_bet)) {
				if (launch_bet_possible(*game)) card_selected = 4;
				else warning_message(screen, "Chaque joueur doit d'abord poser une carte..", 0);
			}
			card_played = card_selected;

		//Le joueur a quitté
		}else if (event.type == SDL_QUIT) {
			*error = 1;
			game->quit = 1;
		}

	}while (card_played == -1 && *error == 0);

	return card_played;
}

void phase_2(SOCKET server, SDL_Surface *screen, Game *game, int my_turn) {	
	char pseudo[MAX_SIZE];
	int bet_condition, bet;

	int confirmation = get_confirmation(server);
	int error = 0;
	int disconnection_player;
	int message_code;

	if (my_turn) {

		if (confirmation) {
			//On récupère les informations sur nos paris
			bet_condition = get_info(server);
		
			//Si on peut parier : on le fait sinon on renvoie l'information 
			if (bet_condition >= 0 && bet_possible(*game, bet_condition)) bet = bet_selection(screen, game, bet_condition + 1, &error);
			else bet = bet_condition;			
					
			//On envoie notre pari
			send_action(server, bet);
		}

		send_error(server, error);
	}

	//Si l'on a pas quitter le jeu
	if (!game->quit) {
		//Si le joueur qui devait jouer n'a pas quitté
		disconnection_player = get_info(server);

		if (!disconnection_player) {
			//On récupère le pseudo du joueur qui a joué
			get_player_action(server, pseudo);

			//On récupère le paris et le code du message
			bet = get_action(server);
			message_code = get_info(server);

			//On affiche le message correspondant
			message(screen, game, pseudo, message_code, bet);

		}else {
			get_player_action(server, pseudo);
			action_message(screen, game, cat_char(pseudo, -1, " quitte le jeu..."));
		}
	}
}

int update_bet(Game game, int mx, int my, Button *button_list, int info, int bet, int *done) {
	int new_bet = bet;

	//On modifie le pari en fonction du bouton sur lequel on appuie
	if (check_button(mx, my, button_list[0]) && new_bet > info) {
		new_bet--;						
		
	}else if (check_button(mx, my, button_list[1]) && new_bet < get_tot_cards_board(game)) {
		new_bet++;						
		
	}else if (check_button(mx, my, button_list[2])) {
		*done = 1;		

	}else if (check_button(mx, my, button_list[3])) {
		new_bet = -1;
		*done = 1;
	}

	return new_bet;
}

int bet_selection(SDL_Surface *screen, Game *game, int info, int *error) {	
	SDL_Event event;	
	int mx = 0, my = 0;

	Button button_list[4];

	int old_bet, bet = info;
	int done = 0;	
	
	display_bet_panel(screen, game, button_list, bet);

	//On nettoie la liste des events
	clear_event(&event);

	do {
		SDL_WaitEvent(&event);
		mx = event.motion.x;		//On récupère les coordonnées du pointeur
		my = event.motion.y;

		//Gestion des events
		if (event.type == SDL_MOUSEBUTTONDOWN) {
			old_bet = bet;
			bet = update_bet(*game, mx, my, button_list, info, bet, &done);

			if (old_bet != bet) display_bet_panel(screen, game, button_list, bet);
		
		//Le joueur a quitté
		}else if (event.type == SDL_QUIT) {
			*error = 1;
			game->quit = 1;
		}

	}while (done == 0 && *error == 0);

	return bet;
}

void phase_3(SOCKET server, SDL_Surface *screen, Game *game, int my_turn) {
	char pseudo[MAX_SIZE];
	int nb_to_flip, player_target_cn, card_type;

	int error = 0;
	int disconnection_player;
	int message_code;

	//Pas besoin de confirmation dans cette phase 

	//On récupère le nombre de carte à retourner et on l'affiche (tous les joueurs le voit)
	nb_to_flip = get_info(server);
	display_flip_panel(screen, game, nb_to_flip);

	if (my_turn) {
		//On choisit la cible et on l'envoie
		player_target_cn = flip_selection(screen, game, &error);
		send_action(server, player_target_cn);

		send_error(server, error);
	}

	//Si l'on a pas quitter le jeu
	if (!game->quit) {
		//Si le joueur qui devait jouer n'a pas quitté
		disconnection_player = get_info(server);

		if (!disconnection_player) {
			//On récupère le pseudo du joueur qui a joué
			get_player_action(server, pseudo);

			//On récupère la cible, le type de la carte et le code du message
			player_target_cn = get_action(server);
			card_type = get_action(server);

			message_code = get_info(server);

			//On affiche une animation et le message correspondant
			flip_animation(screen, game, pseudo, player_target_cn, card_type);
			message(screen, game, pseudo, message_code, 0);

		}else {
			get_player_action(server, pseudo);
			action_message(screen, game, cat_char(pseudo, -1, " quitte le jeu..."));
		}
	}
}

int flip_selection(SDL_Surface *screen, Game *game, int *error) {
	SDL_Event event;
	int mx = 0, my = 0;
	int x, y;

	int nb_players = game->nb_players;
	int player_target_cn = -1;
	int i;

	//On nettoie la liste des events
	clear_event(&event);

	do {
		SDL_WaitEvent(&event);
		mx = event.motion.x;		//On récupère les coordonnées du pointeur
		my = event.motion.y;

		//Gestion des events
		if (event.type == SDL_MOUSEBUTTONDOWN) {
			
			for (i = 0; i < nb_players; i++) {
				x = get_X_coord_player(i, nb_players);
				y = get_Y_coord_player(i, nb_players);

				//On regarde si le joueur a cliqué sur quelqu'un
				if (hover(mx, my, x, y, 110, 110)) {

					//Il reste des cartes au joueur, il doit d'abord les retourner
					if (i > 0 && game->player_list[0].nb_board > 0) warning_message(screen, "Retournez d'abord vos cartes..", 0);
					else if (game->player_list[i].nb_board > 0) player_target_cn = game->player_list[i].cn;
				}
			}

		//Le joueur a quitté
		}else if (event.type == SDL_QUIT) {
			*error = 1;
			game->quit = 1;
		}

	}while (player_target_cn == -1 && *error == 0);

	return player_target_cn;
}

int bet_possible(Game game, int bet) {
	if (bet < get_tot_cards_board(game)) return 1;
	else return 0;
}

void final_phase(SOCKET server, SDL_Surface *screen, Game *game) {
	char pseudo[MAX_SIZE];
	int message_code;
	int eliminated = 0;

	//On récupère le pseudo du joueur et le code du message
	get_player_action(server, pseudo);
	message_code = get_info(server);

	//Si on recoit le message comme quoi on est éliminé	
	if (game->phase == 5) {
		eliminated = get_info(server);
		
		if (eliminated) game->finished = 1;
	}

	//On affiche le message correspondant
	message(screen, game, pseudo, message_code, 0);
}

//Fonctions pour les messages d'actions
void message(SDL_Surface *screen, Game *game, char *pseudo, int message_code, int bet) {
	int phase = game->phase;	

	switch (phase) {
		case 1:
			phase_1_message(screen, game, pseudo, message_code);
			break;
		case 2:
			phase_2_message(screen, game, pseudo, message_code, bet);
			break;
		case 3:
			phase_3_message(screen, game, pseudo, message_code);
			break;
		case 4:
			phase_4_message(screen, game, pseudo, message_code);
			break;
		case 5:
			phase_5_message(screen, game, pseudo, message_code);
			break;
	}
}

void phase_1_message(SDL_Surface *screen, Game *game, char *pseudo, int message_code) {
	switch (message_code) {
		case 1:
			action_message(screen, game, cat_char(pseudo, -1, " joue une carte !"));
			break;
		case 2:
			action_message(screen, game, cat_char(pseudo, -1, " lance les paris !"));
			break;
		case 3:
			action_message(screen, game, cat_char(pseudo, -1, " lance les paris de force !"));
			break;
	}
}

void phase_2_message(SDL_Surface *screen, Game *game, char *pseudo, int message_code, int bet) {
	switch (message_code) {
		case 1:
			action_message(screen, game, cat_char(pseudo, -1, cat_char(" parie ", bet, " !")));
			break;
		case 2:
			action_message(screen, game, cat_char(pseudo, -1, " se couche..."));
			break;
		case 3:
			action_message(screen, game, cat_char(pseudo, -1, " est deja couche..."));
			break;
		case 4:
			action_message(screen, game, cat_char(pseudo, -1, " remporte les paris !"));
			break;
	}
}

void phase_3_message(SDL_Surface *screen, Game *game, char *pseudo, int message_code) {
	switch (message_code) {
		case 1:
			action_message(screen, game, cat_char(pseudo, -1, " retourne une rose !"));
			break;
		case 2:
			action_message(screen, game, cat_char(pseudo, -1, " retourne un skull..."));
			break;
	}
}

void phase_4_message(SDL_Surface *screen, Game *game, char *pseudo, int message_code) {
	switch (message_code) {
		case 1:
			action_message(screen, game, cat_char(pseudo, -1, " remporte le round !"));
			break;
		case 2:
			action_message(screen, game, cat_char(pseudo, -1, " remporte la partie !"));
			break;
	}
}

void phase_5_message(SDL_Surface *screen, Game *game, char *pseudo, int message_code) {
	switch (message_code) {
		case 1:
			action_message(screen, game, cat_char(pseudo, -1, " perd le round..."));
			break;
		case 2:
			action_message(screen, game, cat_char(pseudo, -1, " est elimine..."));
			break;
	}
}

//Fonctions utilitaires
void clear_event(SDL_Event *event) {
	while(SDL_PollEvent(event));
}

//Afin d'avoir les joueurs bien répartis sur le plateau, on utilise un peu de trigonométrie
int get_X_coord_player(int nb, int nb_players) {
	return (int) 500 - (110 / 2) + (400 * cos((pi / 2) + (nb * (2 * pi / nb_players))));
}

int get_Y_coord_player(int nb, int nb_players) {
	return (int) 300 - (110 / 2) + (270 * sin((pi / 2) + (nb * (2 * pi / nb_players))));
}

int get_X_coord_card(int nb, int nb_cards) {
	int w = 200;
	int dx = (1000 / (2 * nb_cards)) - (w / 2);

	return dx + nb * (w + 2 * dx);
}

int get_Y_coord_card(int nb, int nb_cards) {
	int h = 200;
	return (600 / 2) - (h / 2);
}

int get_tot_cards_board(Game game) {
	int nb_players = game.nb_players;	
	int total = 0;
	int i;

	for (i = 0; i < nb_players; i++) {
		total += game.player_list[i].nb_board;
	}
	
	return total;
}

char *get_mat_name(int cn) {
	char name[MAX_SIZE];

	switch (cn) {
		case 1:
			strcpy(name, "Bleu");
			break;
		case 2:
			strcpy(name, "Marron");
			break;
		case 3:
			strcpy(name, "Mauve");
			break;
		case 4:
			strcpy(name, "Orange");
			break;
		case 5:
			strcpy(name, "Rouge");
			break;
		case 6:
			strcpy(name, "Vert");
			break;
	}

	return strdup(name);
}

char *get_mat_front_path(int cn) {
	char path[MAX_SIZE];

	strcpy(path, "res/img/cards/");
	strcat(path, get_mat_name(cn));
	strcat(path, cat_char("_", 4, ".png"));

	return strdup(path);
}

char *get_mat_back_path(int cn) {
	char path[MAX_SIZE];

	strcpy(path, "res/img/cards/");
	strcat(path, get_mat_name(cn));
	strcat(path, cat_char("_", 5, ".png"));

	return strdup(path);
}

char *get_card_back_path(int cn) {
	char path[MAX_SIZE];

	strcpy(path, "res/img/cards/");
	strcat(path, get_mat_name(cn));
	strcat(path, cat_char("_", 2, ".png"));

	return strdup(path);
}

char *get_card_rose_path(int cn) {
	char path[MAX_SIZE];

	strcpy(path, "res/img/cards/");
	strcat(path, get_mat_name(cn));
	strcat(path, cat_char("_", 1, ".png"));

	return strdup(path);
}

char *get_card_skull_path(int cn) {
	char path[MAX_SIZE];

	strcpy(path, "res/img/cards/");
	strcat(path, get_mat_name(cn));
	strcat(path, cat_char("_", 3, ".png"));

	return strdup(path);
}

char *get_card_path(int cn, int type) {
	if (type == 1) return get_card_skull_path(cn);
	else return get_card_rose_path(cn);	
}

Player get_player(char *pseudo, int cn, int is_bot, int nb_hand, int nb_board, int round_w, int round_l) {
	Player temp_player;

	strcpy(temp_player.pseudo, pseudo);
	temp_player.cn = cn;
	temp_player.is_bot = is_bot;

	temp_player.nb_hand = nb_hand;
	temp_player.nb_board = nb_board;

	temp_player.round_w = round_w;
	temp_player.round_l = round_l;

	return temp_player;
}

