#include "hdr/login.h"

int connection_to_server(SOCKET *server, SOCKADDR_IN *sin) {	
	//Fonctions nécessaires pour que le client puisse se connecter au serveur
	*server = socket(AF_INET, SOCK_STREAM, 0);

	if (*server < 0) return -1;

	sin->sin_family = AF_INET;
	sin->sin_addr.s_addr = inet_addr("127.0.0.1");
	sin->sin_port = htons(PORT);

	if (connect(*server, (SOCKADDR *) sin, sizeof(SOCKADDR)) < 0) return -1;
	
	return 0;
}

User get_User(SOCKET server) {
	User temp_user;
	
	//On réceptionne tout les informations sur l'utilisateur (pseudo, avatar, nombre de victoire et de parties jouées)
	wait_message(server, temp_user.pseudo);
	wait_message(server, temp_user.avatar);
	wait_int(server, &temp_user.nb_victory);
	wait_int(server, &temp_user.nb_played);

	return temp_user;
}

int login(SOCKET server, char *pseudo, char *pswd) {
	int login = 0;
	
	//On envoie un message comme quoi on veut se connecter
	send_message(server, "login");
	
	//On envoie notre pseudo et mot de passe
	send_message(server, pseudo);
	send_message(server, pswd);
	
	//On attend une confirmation
	wait_int(server, &login);
	
	return login;
}

void create_new_user(SOCKET server, char *pseudo, char *pswd, char *avatar) {
	//On envoie un message comme quoi on veut s'inscrire
	send_message(server, "register");

	//On envoie notre pseudo, mot de passe et avatar
	send_message(server, pseudo);
	send_message(server, pswd);
	send_message(server, avatar);
}
