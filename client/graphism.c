#include "hdr/graphism.h"

//Fonctions générales
void clear_background(SDL_Surface *screen, int r, int g, int b) {
	SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, r, g, b));
}

void darken_background(SDL_Surface *screen) {
	SDL_Surface *dark_bg = NULL;
	SDL_Rect pos = {0, 0};
	
	dark_bg = SDL_CreateRGBSurface(SDL_HWSURFACE, 1000, 600, 32, 0, 0, 0, 0);
	SDL_FillRect(dark_bg, NULL, SDL_MapRGB(screen->format, 100, 100, 100));
	SDL_SetAlpha(dark_bg, SDL_SRCALPHA, 75);

	SDL_BlitSurface(dark_bg, NULL, screen, &pos);

	SDL_FreeSurface(dark_bg);
}

int hover(int mx, int my, int x, int y, int w, int h) {
	if (mx > x && mx < (x + w) && my > y && my < (y + h)) return 1;
	else return 0;

}

//Fonctions pour les boutons
int get_X(Button button) {
	return button.position.x;
}

int get_Y(Button button) {
	return button.position.y;
}

int get_Width(Button button) {
	return button.position.w;
}

int get_Height(Button button) {
	return button.position.h;
}

void set_Coords(Button *button, int x, int y) {
	button->position.x = x;
	button->position.y = y;
}

void set_Dimension(Button *button, int width, int height) {
	button->position.w = width;
	button->position.h = height;
}

void set_Color(Button *button, int r, int g, int b) {
	button->color.r = r;
	button->color.g = g;
	button->color.b = b;
}

void place_button(SDL_Surface *screen, Button button) {
	SDL_Surface *surface = NULL;

	surface = SDL_CreateRGBSurface(SDL_HWSURFACE, get_Width(button), get_Height(button), 32, 0, 0, 0, 0);
	SDL_FillRect(surface, NULL, SDL_MapRGB(screen->format, button.color.r, button.color.g, button.color.b));
	SDL_BlitSurface(surface, NULL, screen, &button.position);
	
	SDL_FreeSurface(surface);
}

Button create_button(SDL_Surface *screen, int width, int height, int x, int y) {
	Button button;
	
	set_Coords(&button, x, y);
	set_Dimension(&button, width, height);
	set_Color(&button, 254, 255, 239);

	place_button(screen, button);

	return button;
}

Button create_button_color(SDL_Surface *screen, int width, int height, int x, int y, int r, int g, int b) {
	Button button;
	
	set_Coords(&button, x, y);
	set_Dimension(&button, width, height);
	set_Color(&button, r, g, b);

	place_button(screen, button);

	return button;
}

int check_button(int mx, int my, Button button) {
	int click = 0;
	if (mx > get_X(button) && mx < get_X(button) + get_Width(button) && my > get_Y(button) && my < get_Y(button) + get_Height(button)) click = 1;
	return click;
}

//Fonctions pour les images
double get_zoom_X(int wi, int w) {
	double wd = (double) w;	
	return (wd / wi);
}

double get_zoom_Y(int hi, int h) {
	double hd = (double) h;		
	return (double) (hd / hi);
}

void display_image(SDL_Surface *screen, char *path, int x, int y, int width, int height) {
	double zoom_x, zoom_y;	

	SDL_Surface *image_src = NULL, *image_fnl = NULL;
	SDL_Rect pos;

	image_src = IMG_Load(path);
	
	zoom_x = get_zoom_X(image_src->w, width);	
	zoom_y = get_zoom_Y(image_src->h, height);

	image_fnl = rotozoomSurfaceXY(image_src, 0, zoom_x, zoom_y, 1);
	pos.x = x, pos.y = y;

	SDL_BlitSurface(image_fnl, NULL, screen, &pos);
	
	SDL_FreeSurface(image_src);
	SDL_FreeSurface(image_fnl);
}

void semi_flip(SDL_Surface *screen, char *path, int x, int y, int width, int height, int part) {
	double zoom_x, zoom_y;	
	double beginning, ending;	
	double i;

	SDL_Surface *image_src = NULL, *image_fnl = NULL;
	Button support = create_button_color(screen, 2 * width, height, x - width, y, 224, 205, 169);
	SDL_Rect pos;

	image_src = IMG_Load(path);
	
	zoom_x = get_zoom_X(image_src->w, width);	
	zoom_y = get_zoom_Y(image_src->h, height);

	if (part == 1) {
		beginning = zoom_x;
		ending = 0;

	}else {
		beginning = 0;
		ending = -zoom_x;
	}

	for (i = beginning; i > ending; i = i - 0.01) {
		image_fnl = rotozoomSurfaceXY(image_src, 0, i, zoom_y, 1);

		pos.x = x - (image_fnl->w / 2);
		pos.y = y;
		
		place_button(screen, support);
		SDL_BlitSurface(image_fnl, NULL, screen, &pos);
		SDL_Flip(screen);
		SDL_Delay(5);
	}

	SDL_FreeSurface(image_src);
	SDL_FreeSurface(image_fnl);
}

void flip_card(SDL_Surface *screen, char *path_1, char *path_2, int x, int y, int width, int height) {
	semi_flip(screen, path_1, x, y, width, height, 1);
	semi_flip(screen, path_2, x, y, width, height, 2);
}

//Fonctions pour le texte
void write_text_color_size(SDL_Surface *screen, char *text, int x, int y, int r, int g, int b, int size) {
	if (strcmp(text, "") != 0) {	
		SDL_Surface *text_surface = NULL;	
		SDL_Rect pos;

		TTF_Font *font = NULL;
		font = TTF_OpenFont("res/font/Lobster.otf", size);

		SDL_Color color = {r, g, b};

		text_surface = TTF_RenderText_Blended(font, text, color);
		pos.x = x, pos.y = y;

		if (x == -1) pos.x = (screen->w - text_surface->w) / 2;
		if (y == -1) pos.y = (screen->h - text_surface->h) / 2;

		SDL_BlitSurface(text_surface, NULL, screen, &pos);

		SDL_FreeSurface(text_surface);
		TTF_CloseFont(font);
	}
}

void write_text(SDL_Surface *screen, char *text, int x, int y) {
	write_text_color_size(screen, text, x, y, 242, 136, 52, 30);
}

void write_text_color(SDL_Surface *screen, char *text, int x, int y, int r, int g, int b) {
	write_text_color_size(screen, text, x, y, r, g, b, 30);
}

void write_text_size(SDL_Surface *screen, char *text, int x, int y, int size) {
	write_text_color_size(screen, text, x, y, 242, 136, 52, size);
}

void write_text_on_button(SDL_Surface *screen, char *text, Button button) {
	if (strcmp(text, "") != 0) {
		SDL_Surface *text_surface = NULL;	
		SDL_Rect pos;

		TTF_Font *font = NULL;
		font = TTF_OpenFont("res/font/Lobster.otf", 30);

		SDL_Color color = {242, 136, 52};

		text_surface = TTF_RenderText_Blended(font, text, color);
		pos.x = get_X(button) + ((get_Width(button) / 2) - (text_surface->w / 2));
		pos.y = get_Y(button) + ((get_Height(button) / 2) - (text_surface->h / 2));

		SDL_BlitSurface(text_surface, NULL, screen, &pos);
		
		SDL_FreeSurface(text_surface);
		TTF_CloseFont(font);
	}
}

char *cat_char(char *text_1, int nb, char *text_2) {
	char final[MAX_SIZE] = "";
	char buf[BUFFER_SIZE];
	
	//Permet de mieux manipuler les entiers dans les chaines de caractères
	if (strcmp(text_1, "") != 0) strcat(final, text_1);
	if (nb >= 0) {
		sprintf(buf, "%d", nb);
		strcat(final, buf);
	}
	if (strcmp(text_2, "") != 0) strcat(final, text_2);

	return strdup(final);
}

char *hide_text(char *text) {
	char hidden_text[MAX_SIZE];
	
	int i = 0;

	//On créé une nouvelle chaine de même taille que l'ancienne avec que des étoiles
	while (text[i] != '\0') {
		hidden_text[i] = '*';
		i++;
	}

	hidden_text[i] = '\0';

	return strdup(hidden_text);
}


int enter_text(SDL_Surface *screen, Button button, char* text, int hidden) {
	SDL_Event event;
	SDL_EnableUNICODE(1);

	int pos = 0, quit = 0;

	do {
		//On attend un event
		SDL_WaitEvent(&event);
		
		//Le joueur a quitté
		if (event.type == SDL_QUIT) {
				SDL_EnableUNICODE(0);
				quit = -1;

		//Le joueur a appuyé sur une touche
		}else if (event.type == SDL_KEYDOWN) {
			
			//Il s'agit de Backspace, on réduit la chaine de caractère
			if (event.key.keysym.sym == SDLK_BACKSPACE && pos > 0) {
				pos--;
                text[pos] = '\0';
		
			//Il s'agit de Enter, on arrete la saisie
            } else if (event.key.keysym.sym == SDLK_RETURN) {
				SDL_EnableUNICODE(0);
				quit = 1;

			//Il s'agit du lettre ou d'un chiffre, on l'ajoute à la chaine de caractère
            }else if (event.key.keysym.unicode <= 122 && event.key.keysym.unicode >= 48 && pos + 1 < MAX_SIZE) {
				text[pos] = event.key.keysym.unicode;
				pos++;
				text[pos] = '\0';
            }

			//A chaque touche appuyé, il faut actualiser la chaine affichée sur la fenêtre
			place_button(screen, button);			

			if (hidden)	write_text_on_button(screen, hide_text(text), button);
			else write_text_on_button(screen, text, button);
			SDL_Flip(screen);

		//Le joueur a cliqué ailleurs, on arrete la saisie
        }else if (event.type == SDL_MOUSEBUTTONDOWN) {		
			SDL_EnableUNICODE(0);
			quit = 1;
		}

	}while (quit == 0);

	return quit;
}

int text_zone(SDL_Surface *screen, Button button, char *text, int w, int h, int x, int y, int hidden) {
	//On affiche d'une autre couleur la zone de texte (verte)
	button = create_button_color(screen, w, h, x, y, 186, 255, 134);
	SDL_Flip(screen);
	
	//On fait la saisie
	if (enter_text(screen, button, text, hidden) == -1) return -1;

	//On remet la zone de texte normale (blanche)
	button = create_button(screen, w, h, x, y);
	write_text_on_button(screen, (hidden) ? hide_text(text) : text, button);
	SDL_Flip(screen);
		
	return 0;
}

void warning_message(SDL_Surface *screen, char *text, int y_offset) {
	//On affiche un message rouge centré en x et avec un possible offset (pour éviter les superpositions)
	write_text_color(screen, text, -1, 450 + y_offset, 255, 0, 0);
	SDL_Flip(screen);
}

void action_message(SDL_Surface *screen, Game *game, char *text) {	
	reset_board(screen, *game);
	SDL_Flip(screen);
	SDL_Delay(1000);

	//On affiche un message centré correspondant à l'action
	write_text(screen, text, -1, -1);
	SDL_Flip(screen);
	SDL_Delay(1000);
}

